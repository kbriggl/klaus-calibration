/// Devices
#include <VPulseGen.h>
#include <Agilent33250a.h>

/// Std
#include <iostream>
#include <stdlib.h>
#include "string.h"

using namespace std;

int main (int argc, char *argv[]){
	/// Setup power supply
	Agilent33250a* pulser = new Agilent33250a();
	pulser->Setcom("/dev/ttyS1", 9600);

	/// Check if voltage argument is provided
	if(argc<2)
	{
		cout << "Please specify: '"<<argv[0]<<"' state [period]" << endl;
		exit(0);
	}


	/// Output on/off
	if(strcmp(argv[1],"0")==0){
		pulser->Off();
		exit(0);
	}

	if(strcmp(argv[1],"1")!=0){
		cout << "Please specify: '"<<argv[0]<<"' state [period]" << endl;
		exit(0);
	}
	pulser->On();
	

	//period
	if(argc>2)
	{	
		double period = atof(argv[2]);
		cout << "Setting period to '"<<period<<"'" << endl;
		pulser->SetPeriod(period);
	}

}
