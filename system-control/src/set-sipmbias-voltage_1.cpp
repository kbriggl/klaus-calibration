/// Devices
#include <VPowerSupply.h>
#include <Keithley2200.h>
#include <Agilent6614c.h>

/// Std
#include <iostream>
#include <stdlib.h>
#include "string.h"

using namespace std;
void printcurrent(VPowerSupply* power_supply){
		cout << "Current voltage SET: "<<power_supply->GetVolt()<<endl;
		cout << "Current voltage MEAS:"<<power_supply->MeasVolt()<<endl;
}

int main( int argc, char** argv ){
	/// Setup power supply
	VPowerSupply* power_supply = new Agilent6614c();
	power_supply->Setcom("/dev/ttyS1");
	/// Check if voltage argument is provided
	if(argc<2)
	{
		cout << "Please specify output voltage: '"<<argv[0]<<"' state voltage, e.g. ./set-voltage 1 55.5'" << endl;
		cout << "	Voltage value needed if state==1" << endl;
		printcurrent(power_supply);
		exit(0);
	}


	/// Output on/off
	if(strcmp(argv[1],"0")==0){
		power_supply->Off();
		exit(0);
	}

	if(strcmp(argv[1],"1")!=0){
		cout << "Please specify output voltage: './set-voltage state voltage, e.g. ./set-voltage 1 55.5'" << endl;
		exit(0);
	}
	
	if(argc<3)
	{
		cout << "Please specify output voltage: './set-voltage state [voltage], e.g. ./set-voltage 1 55.5'" << endl;
		cout << "	Voltage value needed if state==1" << endl;
		exit(0);
	}


	double volt = atof(argv[2]);
	if (volt < 0 || volt > 45){
		cout << "Given voltage"<<volt<<" is out of safe range, please check" << endl;
		exit(0);
	}
	

	power_supply->SetVolt(volt);
	power_supply->On();

	/// Current voltage
	printcurrent(power_supply);

	delete power_supply;
}

