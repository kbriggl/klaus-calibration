//Command to execute the analysis script. Check correct location of data and execution!!!
//root -l -e ".L ~/git-repos/KLauS-DAQ/Lab-DAQ-Raspi/config-common/libKLauSconfig.so" -e ".L ~/git-repos/KLauS-DAQ/Lab-DAQ-Raspi/daq-common/klaus_storagetypes.so" -e ".L ~/klau6-testbeam-1020/klaus-calibration/sweeps/common/libana-common.so" timewalk_CalibBox.C  -q

void timewalk_CalibBox(){
TFile fin("Run8.root");
//INPUT
TTree* t=(TTree*) fin.Get("tr_hits");
klaus_acquisition* acq=NULL;
t->SetBranchAddress("acquisitions",&acq);
unsigned short thres,last_thres=255;
t->SetBranchAddress("thresh",&thres);
double vHV1,last_vHV1=255;
t->SetBranchAddress("vHV1",&vHV1);

//OUTPUT
TFile fout("timewalk_CalibBox_Run9.root","recreate");
TH1F hDT("hDT","hDT1",1200,-600,600);
TH1F hE1("hE1","hE1",400,600,1000);
TH1F hE2("hE2","hE2",400,600,1000);
TH2F hDTtw("hDTtw","hDT1tw",400,600,1000,400,-200,200);
TH1F hE1_corr("hE1_corr","hE1_corr",400,600,1000);
TH1F hE2_corr("hE2_corr","hE2_corr",400,600,1000);

TTree tout("tout","tout");
tout.Branch("hDT",&hDT);
tout.Branch("hE1",&hE1);
tout.Branch("hE2",&hE2);
tout.Branch("hDTtw",&hDTtw);
tout.Branch("thres",&last_thres);
tout.Branch("vHV1",&last_vHV1);
float sigma;
tout.Branch("sigma",&sigma);
//Not TDC Corrected Graphs and TH2Fs
//Graphs
TGraphErrors gRMS_vHV1;
TGraphErrors gRMS_thres;
//Initialize enough graphs for checking RMS and sigma
TH2F hMap_Sigma_thresh_vHV1("hMap_Sigma","hMap_Sigma",5,0,5,10,56,61);
TH2F hMap_RMS_thresh_vHV1("hMap_Sigma","hMap_Sigma",5,0,5,10,56,61);
hMap_Sigma_thresh_vHV1.GetXaxis()->SetTitle("threshold");
hMap_RMS_thresh_vHV1.GetXaxis()->SetTitle("threshold");
hMap_Sigma_thresh_vHV1.GetYaxis()->SetTitle("vHV1");
hMap_RMS_thresh_vHV1.GetYaxis()->SetTitle("vHV1");


//Initialize TH2Fs for optimized settings
TH2F hMap_opt_RMS_thresh_vHV1("hMap_opt_RMS","hMap_opt_RMS",5,0,5,10,56,61);
TH2F hMap_opt_Sigma_thresh_vHV1("hMap_opt_Sigma","hMap_opt_Sigma",5,0,5,10,56,61);
hMap_opt_Sigma_thresh_vHV1.GetXaxis()->SetTitle("threshold");
hMap_opt_RMS_thresh_vHV1.GetXaxis()->SetTitle("threshold");
hMap_opt_Sigma_thresh_vHV1.GetYaxis()->SetTitle("vHV1");
hMap_opt_RMS_thresh_vHV1.GetYaxis()->SetTitle("vHV1");

//TDC Corrected Graphs and TH2Fs
//Graphs
TGraphErrors gRMS_vHV1_corr;
TGraphErrors gRMS_thres_corr;
//Initialize enough graphs for checking RMS and sigmaT
TH2F hMap_Sigma_thresh_vHV1_corr("hMap_Sigma_corr","hMap_Sigma_corr",5,0,5,10,56,61);
TH2F hMap_RMS_thresh_vHV1_corr("hMap_Sigma_corr","hMap_Sigma_corr",5,0,5,10,56,61);
hMap_Sigma_thresh_vHV1_corr.GetXaxis()->SetTitle("threshold");
hMap_RMS_thresh_vHV1_corr.GetXaxis()->SetTitle("threshold");
hMap_Sigma_thresh_vHV1_corr.GetYaxis()->SetTitle("vHV1");
hMap_RMS_thresh_vHV1_corr.GetYaxis()->SetTitle("vHV1");


//Initialize TH2Fs for optimized settings
TH2F hMap_opt_RMS_thresh_vHV1_corr("hMap_opt_RMS_corr","hMap_opt_RMS_corr",5,0,5,10,56,61);
TH2F hMap_opt_Sigma_thresh_vHV1_corr("hMap_opt_Sigma_corr","hMap_opt_Sigma_corr",5,0,5,10,56,61);
hMap_opt_Sigma_thresh_vHV1_corr.GetXaxis()->SetTitle("threshold");
hMap_opt_RMS_thresh_vHV1_corr.GetXaxis()->SetTitle("threshold");
hMap_opt_Sigma_thresh_vHV1_corr.GetYaxis()->SetTitle("vHV1");
hMap_opt_RMS_thresh_vHV1_corr.GetYaxis()->SetTitle("vHV1");




TF1 *ftw = new TF1("ftw","1/expo",600,1000);//For timewalk correction
TF1 f("f","gaus",-100,100);//For timing resolution

//Uncorrected TREEWALK
auto nE=t->GetEntries();
for(int e=0;e<nE;e++){
	t->GetEntry(e);
	if(e%100==99){printf("%3.2f%%\r",e*100./nE);fflush(stdout);}
	bool new_point=false;
	if(last_thres!=255 && last_thres!=thres){
		//printf("Last Thresh:%u New:%u\n",last_thres,thres);
		new_point=true;
	}
	if(last_vHV1!=255 && last_vHV1!=vHV1){
		//printf("Last HV:%e New:%e\n",last_vHV1,vHV1);	
		new_point=true;
	}
	if(new_point){
		//store
		fout.mkdir(TString::Format("vHV1-%f_thres%u",last_vHV1,last_thres))->cd();
		hDT.Fit(&f,"QR","",-20,20);
		sigma=f.GetParameter(2);
		hDTtw.Fit(ftw,"QR","",720,850);
		printf("vHV1: %f, Thresh:%u Sigma of uncorrected Spectrum: %f\n",last_vHV1,last_thres,sigma);
		hDT.Write();
		hE1.Write();
		hE2.Write();
		hDTtw.Write();
		gRMS_vHV1.SetPoint(gRMS_vHV1.GetN(),last_vHV1,hDT.GetRMS());
		gRMS_thres.SetPoint(gRMS_thres.GetN(),last_thres,hDT.GetRMS());
		tout.Fill();
		
		//Fill TH2F
                hMap_Sigma_thresh_vHV1.Fill(last_thres,last_vHV1,sigma);
                hMap_RMS_thresh_vHV1.Fill(last_thres,last_vHV1,hDT.GetRMS());
                //Fill optimized TH2F
                //Sigma Maps
                int bin;
                bin=hMap_opt_Sigma_thresh_vHV1.FindBin(last_thres,last_vHV1);
                if(hMap_opt_Sigma_thresh_vHV1.GetBinContent(bin)==0 || hMap_opt_Sigma_thresh_vHV1.GetBinContent(bin)>sigma)
                        hMap_opt_Sigma_thresh_vHV1.SetBinContent(bin,sigma);
		//RMS Maps
		bin=hMap_opt_RMS_thresh_vHV1.FindBin(last_thres,last_vHV1);
                if(hMap_opt_RMS_thresh_vHV1.GetBinContent(bin)==0 || hMap_opt_RMS_thresh_vHV1.GetBinContent(bin)>hDT.GetRMS())
                        hMap_opt_RMS_thresh_vHV1.SetBinContent(bin,hDT.GetRMS());



		hDT.Reset();
		hE1.Reset();
		hE2.Reset();
		hDTtw.Reset();
	}

	last_thres=thres;
	last_vHV1=vHV1;

	std::list<klaus_event> hitsA;	
	std::list<klaus_event> hitsB;	
	for(klaus_event& hit: acq->data.begin()->second){
		if(hit.channel==4){
			hitsA.emplace_back(hit);
			if(hit.gainsel_evt) continue;
			hE1.Fill(hit.ADC_10b);
		}
		if(hit.channel==22){
			hitsB.emplace_back(hit);
			if(hit.gainsel_evt) continue;
			hE2.Fill(hit.ADC_10b);
		}
	}
	for(auto hitA: hitsA)
		for(auto hitB:hitsB){
			hDT.Fill(hitA.DiffTime(hitB));
			hDTtw.Fill(hitA.ADC_10b,hitA.DiffTime(hitB));
		}
}
/////////////////////---------------------TDC&ADC&timewalk correction------------------------------///////////////////////
        //Use Calibration Box for DNL Correction 
        CalibrationBox* calib = new CalibrationBox(36,160e6,7); //Calibration Box 
        fout.cd();
        uint32_t realtime;
//	int32_t realcharge;
        cout<<"Constructed Calib Box"<<endl;
        for(int i=0;i<t->GetEntries();i++){
                t->GetEntry(i);
                calib->AddHits(*acq);
        }
	calib->FixBrokenBins();
        calib->RecomputeTimeDNL();
        calib->Write();  
	calib->ReadADCLinearity_db("../analysed-sar-Run2.root");
	calib->SetPedestal(4,708.3,true);
	calib->SetPedestal(22,710.4,true);
	calib->SetAGScaling(4,32);
	calib->SetAGScaling(22,32);
	fout.cd();
       	calib->GetTimeCountsHist().Write();
/*	for(int i=0;i<36;i++){
		if(calib->GetTimeBinWidthHist(i).GetEntries()==0) continue; //ignore empty/unused channels
 		calib->GetTimeBinWidthHist(i).Write();
        }
	//Get DNL histos. Time consuming due to additional treewalk
	cout<<"DNL histos, going to Bin dithering"<<endl; //optional
	for(int j=1; j<36;j+=3){//j is channel
        TH1D*  TDC_DNL_BDITH    = new TH1D(TString::Format("TDC_DNL_BDITH_Ch%u",j),"TDC_DNL_BDITH",1<<9,0,1<<9); 
        TH1D*  Time	        = new TH1D(TString::Format("Time_Ch%u",j),"Time",128,0,128);
	TH2F* CheckTime 	= new TH2F(TString::Format("CheckTime_Ch%u",j),"CheckTime",128,0,128,564,0,564);	
	CheckTime->GetXaxis()->SetTitle("Time");
	CheckTime->GetYaxis()->SetTitle("Realtime");
	double mean = calib->GetTimeCountsHist().GetBinContent(j+1)/(1<<9);
//	TH1D* ADC_DNL_BDITH	= new TH1D(TString::Format("ADC_DNL_BDITH_Ch%u",j),"ADC_DNL_BDITH",4096,0,4096);	
//	printf("Mean %f\n",mean);
//	printf("Bin dihering Events of channel %u \n",j);	
        
	for(int i=0;i<t->GetEntries();i++){//i is acquisition_nr
                t->GetEntry(i);
                for(auto hitlist: acq->data){
                        for(auto hit: hitlist.second){
                                if(hit.channel == j){
				//Sort out 128bin - periodicity
				Time->Fill(hit.GetTime()%128);
				realtime = calib->GetTimeDNLCorrected_BDITH(hit,false);
//				realcharge = calib->GetChargeDNLCorrected_BDITH(hit,4,true,false);
//			      	ADC_DNL_BDITH->Fill(realcharge);	
				if(hit.GetTime()%128 ==31||hit.GetTime()%128==33||hit.GetTime()%128==32||hit.GetTime()%128==34){// continue;
				TDC_DNL_BDITH->SetBinContent(realtime%(1<<9),mean);
			}
				else{
				TDC_DNL_BDITH->Fill(realtime%(1<<9));
				}
				CheckTime->Fill(hit.GetTime()%128,realtime%(1<<9));
                        	}
                	}
		}
        }
	Time->Write();
        TDC_DNL_BDITH->Write();
//	ADC_DNL_BDITH->Write();
	CheckTime->Write();
	}*/
//second treewalk for ADC&TDC DNL and timewalk correction 
TH1F hDT_corr("hDT","hDT1",1200,-600,600);
TH2F hDTtw_corr("hDTtw","hDT1tw",400,600,1000,400,-200,200);
TH2F hDTtw_final("hDTtw_corr","hDT1tw_corr",400,600,1000,400,-200,200);
TH1F hDT_final("hDT_final","hDT1_final",1200,-600,600);

TF1 *ftw2 = new TF1("ftw2","1/expo",600,1000);//For timewalk correction
TF1 f2("f2","gaus",-100,100);
TF1 f3("f3","gaus",-100,100);

tout.Branch("hDT",&hDT);
tout.Branch("hDTtw",&hDTtw);

last_vHV1=255;
last_thres=255;
auto nE_corr=t->GetEntries();
for(int e=0;e<nE_corr;e++){
	t->GetEntry(e);
	if(e%100==99){printf("%3.2f%%\r",e*100./nE);fflush(stdout);}
	bool new_point=false;
	if(last_thres!=255 && last_thres!=thres){
		new_point=true;
	}
	if(last_vHV1!=255 && last_vHV1!=vHV1){
		new_point=true;
	}
	if(new_point){
		//store
		fout.mkdir(TString::Format("vHV1-%f_thres%u_TDCcorr",last_vHV1,last_thres))->cd();
		hDT_corr.Fit(&f2,"QR","",-20,20);
		double sigma_corr=f2.GetParameter(2);
		printf("vHV1: %f, Thresh:%u Sigma of TDC corrected Spectrum: %f\n",last_vHV1,last_thres,sigma_corr);	
		hDTtw_corr.Fit(ftw2,"QR","",720,850);
		hDT_final.Fit(&f3,"QR","",-20,20);
		double sigma_final=f3.GetParameter(2);
		printf("vHV1: %f, Thresh:%u Sigma of TDC and timewalk corrected Spectrum: %f\n",last_vHV1,last_thres,sigma_final);	
		hE1_corr.Write();
		hE2_corr.Write();
		hDT_corr.Write();
		hDTtw_corr.Write();
		hDTtw_final.Write();
		hDT_final.Write();

	        gRMS_vHV1_corr.SetPoint(gRMS_vHV1.GetN(),last_vHV1,hDT_corr.GetRMS());
                gRMS_thres_corr.SetPoint(gRMS_thres.GetN(),last_thres,hDT_corr.GetRMS());
                tout.Fill();

                //Fill TH2F
                hMap_Sigma_thresh_vHV1_corr.Fill(last_thres,last_vHV1,sigma_corr);
                hMap_RMS_thresh_vHV1_corr.Fill(last_thres,last_vHV1,hDT_corr.GetRMS());
                //Fill optimized TH2F
                //Sigma Maps
                int bin;
                bin=hMap_opt_Sigma_thresh_vHV1_corr.FindBin(last_thres,last_vHV1);
                if(hMap_opt_Sigma_thresh_vHV1_corr.GetBinContent(bin)==0 || hMap_opt_Sigma_thresh_vHV1.GetBinContent(bin)>sigma_corr)
                        hMap_opt_Sigma_thresh_vHV1_corr.SetBinContent(bin,sigma_corr);
                //RMS Maps
                bin=hMap_opt_RMS_thresh_vHV1_corr.FindBin(last_thres,last_vHV1);
                if(hMap_opt_RMS_thresh_vHV1_corr.GetBinContent(bin)==0 || hMap_opt_RMS_thresh_vHV1.GetBinContent(bin)>hDT_corr.GetRMS())
                        hMap_opt_RMS_thresh_vHV1_corr.SetBinContent(bin,hDT_corr.GetRMS());


		hE1_corr.Reset();
		hE2_corr.Reset();
		hDT_corr.Reset();
		hDTtw_corr.Reset();
		hDTtw_final.Reset();
		hDT_final.Reset();
	}

	last_thres=thres;
	last_vHV1=vHV1;
	
	std::list<klaus_event> hitsA_corr;	
	std::list<klaus_event> hitsB_corr;	
	for(klaus_event& hit: acq->data.begin()->second){
		if(hit.channel==4){
			hitsA_corr.emplace_back(hit);
			hE1_corr.Fill(calib->GetChargeDNLCorrected_BDITH(hit,4,true,true)/4);
		}
		if(hit.channel==22){
			hitsB_corr.emplace_back(hit);
			hE2_corr.Fill(calib->GetChargeDNLCorrected_BDITH(hit,4,true,true)/4);
		}
	}
	for(auto hitA_corr: hitsA_corr){
		int timeA = hitA_corr.GetTime()%128;
	//	if(timeA==31||timeA==32||timeA==33||timeA==34) continue;
		int32_t chargeA =  calib->GetChargeDNLCorrected_BDITH(hitA_corr,4,true,true)/4;//ADC DNL Correction
		for(auto hitB_corr:hitsB_corr){
			int timeB = hitB_corr.GetTime()%128;
	//		if(timeB ==31||timeB==32||timeB==33||timeB==34) continue;
			signed int Tdiff =calib->GetTimeDNLCorrected_BDITH(hitA_corr,false)-calib->GetTimeDNLCorrected_BDITH(hitB_corr,false);
			hDT_corr.Fill(Tdiff/4);
		
			hDTtw_corr.Fill(chargeA,Tdiff/4);
			
			hDTtw_final.Fill(chargeA,Tdiff/4-(ftw2->Eval(chargeA)));//Timewalk correction
			hDT_final.Fill(Tdiff/4-(ftw2->Eval(chargeA)));
			
			 
		}
	}
}







//store last point
fout.mkdir(TString::Format("vHV1%f_thres%u",last_vHV1,last_thres))->cd();
hDT.Fit(&f,"QR","",-20,20);
sigma=f.GetParameter(2);
hDT.Write();
hE1.Write();
hE2.Write();
hDTtw.Write();
gRMS_vHV1.SetPoint(gRMS_vHV1.GetN(),last_vHV1,hDT.GetRMS());
gRMS_thres.SetPoint(gRMS_thres.GetN(),last_thres,hDT.GetRMS());


fout.mkdir(TString::Format("vHV1%f_thres%u_corr",last_vHV1,last_thres))->cd();
hDT_corr.Fit(&f2,"QR","",-20,20);
double sigma_corr = f2.GetParameter(2);
hDT_corr.Write();
hDTtw_corr.Write();
hDTtw_final.Write();

//store global results
fout.cd();
//TDC uncorrected
gRMS_vHV1.SetNameTitle("gDT_vHV1_RMS","gTD_RMS");
gRMS_vHV1.GetYaxis()->SetTitle("RMS(dT)");
gRMS_vHV1.Write();

gRMS_thres.SetNameTitle("gDT_thres_RMS","gTD_RMS");
gRMS_thres.GetYaxis()->SetTitle("RMS(dT)");
gRMS_thres.Write();

tout.Write();
hMap_Sigma_thresh_vHV1.Write();
hMap_RMS_thresh_vHV1.Write();
hMap_opt_Sigma_thresh_vHV1.Write();
hMap_opt_RMS_thresh_vHV1.Write();
//Print Color Maps for DAC optimization

auto c2=new TCanvas("NOT TDC corrected Canvas","NOT TDC corrected Canvas");
c2->Divide(1,2);
c2->cd(1);
//gRMS_vHV1.DrawClone("AP*");
//c2->cd(2);
//gRMS_thres.DrawClone("AP*");
c2->cd(1);
hMap_opt_Sigma_thresh_vHV1.DrawCopy("colz text");
c2->cd(2);
hMap_opt_RMS_thresh_vHV1.DrawCopy("colz text");

//TDC corrected
gRMS_vHV1_corr.SetNameTitle("gDT_vHV1_RMS_corr","gTD_RMS_corr");
gRMS_vHV1_corr.GetYaxis()->SetTitle("RMS(dT)");
gRMS_vHV1_corr.Write();

gRMS_thres_corr.SetNameTitle("gDT_thres_RMS","gTD_RMS");
gRMS_thres_corr.GetYaxis()->SetTitle("RMS(dT)");
gRMS_thres_corr.Write();


hMap_Sigma_thresh_vHV1_corr.Write();
hMap_RMS_thresh_vHV1_corr.Write();
hMap_opt_Sigma_thresh_vHV1_corr.Write();
hMap_opt_RMS_thresh_vHV1_corr.Write();

auto c3=new TCanvas("TDC corrected Canvas","TDC corrected Canvas");
c3->Divide(1,2);
//c3->cd(1);
//gRMS_vHV1_corr.DrawClone("AP*");
//c3->cd(2);
//gRMS_thres_corr.DrawClone("AP*");
c3->cd(1);
hMap_opt_Sigma_thresh_vHV1_corr.DrawCopy("colz text");
c3->cd(2);
hMap_opt_RMS_thresh_vHV1_corr.DrawCopy("colz text");



}
