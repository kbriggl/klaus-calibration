


################### Introduction to subdirectoy HV_TThreshold ###############

Here are the scan and analysis script for the TThreshold - HV measurement. It is used to find an optimal point of these DACs for timing, since they are the most crucial.
The evaluation of the analysis consists of an optimized map of sigmas, which are fits to a TDiff spectrum after TDC DNL correction. 
The user is advised to decide for himself, for what criteria he wants to optimize. 

The scan iterates through all DAC combinations, so the measurement should be carried out overnight at testbeam.

The analysis works like the following:

getRAW: Uses uncorrected data, without DNL correction. 

getCalibrationBox: Needs to be run in order to use DNL corrected data for ADC and TDC. Has to be executed before the next steps can be done.

get_timewalk_CalibBox_TB06/21: Routine to initialize graphs and maps, include CalibrationBox, build TDiff spectrums for certain channel pairs, fit them and put results in Graphs and maps.
Important: Check initialized and set ranges of histograms! 

Execution of all scripts is explained at the top of the scripts.

Last Change: E. Warttmann, 08.02.2022

