void timewalk(){
TFile fin("Run9.root");
//INPUT
TTree* t=(TTree*) fin.Get("tr_hits");
klaus_acquisition* acq=NULL;
t->SetBranchAddress("acquisitions",&acq);
unsigned short thres,last_thres=255;
t->SetBranchAddress("thresh",&thres);
unsigned short ISGbias,last_ISGbias=255;
t->SetBranchAddress("ISGbias",&ISGbias);

//OUTPUT
TFile fout("timewalk.root","recreate");
TH1F hDT("hDT","hDT1",1200,-600,600);
TH1F hE1("hE1","hE1",400,600,1000);
TH1F hE2("hE2","hE2",400,600,1000);
TH2F hDTtw("hDTtw","hDT1tw",400,600,1000,400,-200,200);
//Global output
TGraphErrors gRMS_ISGbias;
TGraphErrors gRMS_thres;
TH2F hMap_RMS("hMap_RMS","hMap_RMS",5,0,5,4,0,4);
TH2F hMap_Sigma("hMap_Sigma","hMap_Sigma",5,0,5,4,0,4);
hMap_Sigma.GetXaxis()->SetTitle("threshold");
hMap_RMS.GetXaxis()->SetTitle("threshold");
hMap_Sigma.GetYaxis()->SetTitle("ISGbias");
hMap_RMS.GetYaxis()->SetTitle("ISGbias");

TTree tout("tout","tout");
tout.Branch("hDT",&hDT);
tout.Branch("hE1",&hE1);
tout.Branch("hE2",&hE2);
tout.Branch("hDTtw",&hDTtw);
tout.Branch("thres",&last_thres);
tout.Branch("ISGbias",&last_ISGbias);
float sigma;
tout.Branch("sigma",&sigma);

TF1 f("f","gaus",-100,100);

//TREEWALK
auto nE=t->GetEntries();
for(int e=0;e<nE;e++){
	t->GetEntry(e);
	if(e%100==99){printf("%3.2f%%\r",e*100./nE);fflush(stdout);}
	bool new_point=false;
	if(last_thres!=255 && last_thres!=thres){
		//printf("Last Thresh:%u New:%u\n",last_thres,thres);
		new_point=true;
	}
	if(last_ISGbias!=255 && last_ISGbias!=ISGbias){
		//printf("Last HV:%e New:%e\n",last_ISGbias,ISGbias);	
		new_point=true;
	}
	if(new_point){
		//store
		fout.mkdir(TString::Format("ISGbias-%u_thres%u",last_ISGbias,last_thres))->cd();
		hDT.Fit(&f,"QR","",-20,20);
		sigma=f.GetParameter(2);
		hDT.Write();
		hE1.Write();
		hE2.Write();
		hDTtw.Write();
		gRMS_ISGbias.SetPoint(gRMS_ISGbias.GetN(),last_ISGbias,hDT.GetRMS());
		gRMS_thres.SetPoint(gRMS_thres.GetN(),last_thres,hDT.GetRMS());
		tout.Fill();
		hMap_Sigma.Fill(last_thres,last_ISGbias,sigma);
		hMap_RMS.Fill(last_thres,last_ISGbias,hDT.GetRMS());
		hDT.Reset();
		hE1.Reset();
		hE2.Reset();
		hDTtw.Reset();
	}

	last_thres=thres;
	last_ISGbias=ISGbias;

	std::list<klaus_event> hitsA;	
	std::list<klaus_event> hitsB;	
	for(klaus_event& hit: acq->data.begin()->second){
		if(hit.channel==4){
			hitsA.emplace_back(hit);
			if(hit.gainsel_evt) continue;
			hE1.Fill(hit.ADC_10b);
		}
		if(hit.channel==22){
			hitsB.emplace_back(hit);
			if(hit.gainsel_evt) continue;
			hE2.Fill(hit.ADC_10b);
		}
	}
	for(auto hitA: hitsA)
		for(auto hitB:hitsB){
			hDT.Fill(hitA.DiffTime(hitB));
			hDTtw.Fill(hitA.ADC_10b,hitA.DiffTime(hitB));
		}
}
//store last point
fout.mkdir(TString::Format("ISGbias%u_thres%u",last_ISGbias,last_thres))->cd();
hDT.Fit(&f,"QR","",-20,20);
sigma=f.GetParameter(2);
hDT.Write();
hE1.Write();
hE2.Write();
hDTtw.Write();
gRMS_ISGbias.SetPoint(gRMS_ISGbias.GetN(),last_ISGbias,hDT.GetRMS());
gRMS_thres.SetPoint(gRMS_thres.GetN(),last_thres,hDT.GetRMS());

//store global results
fout.cd();
gRMS_ISGbias.SetNameTitle("gDT_ISGbias_RMS","gTD_RMS");
gRMS_ISGbias.GetYaxis()->SetTitle("RMS(dT)");
gRMS_ISGbias.Write();

gRMS_thres.SetNameTitle("gDT_thres_RMS","gTD_RMS");
gRMS_thres.GetYaxis()->SetTitle("RMS(dT)");
gRMS_thres.Write();
tout.Write();

hMap_Sigma.Write();
hMap_RMS.Write();

//auto c1=new TCanvas("c1");
//c1->Divide(3,1);
//c1->cd(1);
//hDT.DrawCopy();
//c1->cd(2);
//hE1.DrawCopy();
//c1->cd(3);
//hE2.DrawCopy();
auto c2=new TCanvas("c2");
//hDTtw.DrawCopy("colz");
c2->Divide(2,2);
c2->cd(1);
gRMS_ISGbias.DrawClone("AP*");
c2->cd(2);
gRMS_thres.DrawClone("AP*");
c2->cd(3);
hMap_Sigma.DrawCopy("colz text");
c2->cd(4);
hMap_RMS.DrawCopy("colz text");
}
