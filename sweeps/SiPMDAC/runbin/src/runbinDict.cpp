// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME srcdIrunbinDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "../../common//include/CalibreADC.h"
#include "../../common//include/spsGainExtractor.h"
#include "../../common//include/klaus_constants.h"
#include "../../common//include/EventType.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *CalibreADC_Dictionary();
   static void CalibreADC_TClassManip(TClass*);
   static void *new_CalibreADC(void *p = 0);
   static void *newArray_CalibreADC(Long_t size, void *p);
   static void delete_CalibreADC(void *p);
   static void deleteArray_CalibreADC(void *p);
   static void destruct_CalibreADC(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CalibreADC*)
   {
      ::CalibreADC *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CalibreADC));
      static ::ROOT::TGenericClassInfo 
         instance("CalibreADC", "../../common//include/CalibreADC.h", 11,
                  typeid(::CalibreADC), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CalibreADC_Dictionary, isa_proxy, 4,
                  sizeof(::CalibreADC) );
      instance.SetNew(&new_CalibreADC);
      instance.SetNewArray(&newArray_CalibreADC);
      instance.SetDelete(&delete_CalibreADC);
      instance.SetDeleteArray(&deleteArray_CalibreADC);
      instance.SetDestructor(&destruct_CalibreADC);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CalibreADC*)
   {
      return GenerateInitInstanceLocal((::CalibreADC*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CalibreADC*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalibreADC_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CalibreADC*)0x0)->GetClass();
      CalibreADC_TClassManip(theClass);
   return theClass;
   }

   static void CalibreADC_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *spsGainExtractor_Dictionary();
   static void spsGainExtractor_TClassManip(TClass*);
   static void *new_spsGainExtractor(void *p = 0);
   static void *newArray_spsGainExtractor(Long_t size, void *p);
   static void delete_spsGainExtractor(void *p);
   static void deleteArray_spsGainExtractor(void *p);
   static void destruct_spsGainExtractor(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::spsGainExtractor*)
   {
      ::spsGainExtractor *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::spsGainExtractor));
      static ::ROOT::TGenericClassInfo 
         instance("spsGainExtractor", "../../common//include/spsGainExtractor.h", 15,
                  typeid(::spsGainExtractor), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &spsGainExtractor_Dictionary, isa_proxy, 0,
                  sizeof(::spsGainExtractor) );
      instance.SetNew(&new_spsGainExtractor);
      instance.SetNewArray(&newArray_spsGainExtractor);
      instance.SetDelete(&delete_spsGainExtractor);
      instance.SetDeleteArray(&deleteArray_spsGainExtractor);
      instance.SetDestructor(&destruct_spsGainExtractor);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::spsGainExtractor*)
   {
      return GenerateInitInstanceLocal((::spsGainExtractor*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::spsGainExtractor*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *spsGainExtractor_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::spsGainExtractor*)0x0)->GetClass();
      spsGainExtractor_TClassManip(theClass);
   return theClass;
   }

   static void spsGainExtractor_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_klaus_event(void *p = 0);
   static void *newArray_klaus_event(Long_t size, void *p);
   static void delete_klaus_event(void *p);
   static void deleteArray_klaus_event(void *p);
   static void destruct_klaus_event(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::klaus_event*)
   {
      ::klaus_event *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::klaus_event >(0);
      static ::ROOT::TGenericClassInfo 
         instance("klaus_event", ::klaus_event::Class_Version(), "../../common//include/EventType.h", 20,
                  typeid(::klaus_event), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::klaus_event::Dictionary, isa_proxy, 4,
                  sizeof(::klaus_event) );
      instance.SetNew(&new_klaus_event);
      instance.SetNewArray(&newArray_klaus_event);
      instance.SetDelete(&delete_klaus_event);
      instance.SetDeleteArray(&deleteArray_klaus_event);
      instance.SetDestructor(&destruct_klaus_event);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::klaus_event*)
   {
      return GenerateInitInstanceLocal((::klaus_event*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::klaus_event*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_klaus_acquisition(void *p = 0);
   static void *newArray_klaus_acquisition(Long_t size, void *p);
   static void delete_klaus_acquisition(void *p);
   static void deleteArray_klaus_acquisition(void *p);
   static void destruct_klaus_acquisition(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::klaus_acquisition*)
   {
      ::klaus_acquisition *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::klaus_acquisition >(0);
      static ::ROOT::TGenericClassInfo 
         instance("klaus_acquisition", ::klaus_acquisition::Class_Version(), "../../common//include/EventType.h", 61,
                  typeid(::klaus_acquisition), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::klaus_acquisition::Dictionary, isa_proxy, 4,
                  sizeof(::klaus_acquisition) );
      instance.SetNew(&new_klaus_acquisition);
      instance.SetNewArray(&newArray_klaus_acquisition);
      instance.SetDelete(&delete_klaus_acquisition);
      instance.SetDeleteArray(&deleteArray_klaus_acquisition);
      instance.SetDestructor(&destruct_klaus_acquisition);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::klaus_acquisition*)
   {
      return GenerateInitInstanceLocal((::klaus_acquisition*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::klaus_acquisition*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_klaus_cec_data(void *p = 0);
   static void *newArray_klaus_cec_data(Long_t size, void *p);
   static void delete_klaus_cec_data(void *p);
   static void deleteArray_klaus_cec_data(void *p);
   static void destruct_klaus_cec_data(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::klaus_cec_data*)
   {
      ::klaus_cec_data *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::klaus_cec_data >(0);
      static ::ROOT::TGenericClassInfo 
         instance("klaus_cec_data", ::klaus_cec_data::Class_Version(), "../../common//include/EventType.h", 81,
                  typeid(::klaus_cec_data), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::klaus_cec_data::Dictionary, isa_proxy, 4,
                  sizeof(::klaus_cec_data) );
      instance.SetNew(&new_klaus_cec_data);
      instance.SetNewArray(&newArray_klaus_cec_data);
      instance.SetDelete(&delete_klaus_cec_data);
      instance.SetDeleteArray(&deleteArray_klaus_cec_data);
      instance.SetDestructor(&destruct_klaus_cec_data);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::klaus_cec_data*)
   {
      return GenerateInitInstanceLocal((::klaus_cec_data*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::klaus_cec_data*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr klaus_event::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *klaus_event::Class_Name()
{
   return "klaus_event";
}

//______________________________________________________________________________
const char *klaus_event::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_event*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int klaus_event::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_event*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *klaus_event::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_event*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *klaus_event::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_event*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr klaus_acquisition::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *klaus_acquisition::Class_Name()
{
   return "klaus_acquisition";
}

//______________________________________________________________________________
const char *klaus_acquisition::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_acquisition*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int klaus_acquisition::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_acquisition*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *klaus_acquisition::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_acquisition*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *klaus_acquisition::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_acquisition*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr klaus_cec_data::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *klaus_cec_data::Class_Name()
{
   return "klaus_cec_data";
}

//______________________________________________________________________________
const char *klaus_cec_data::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_cec_data*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int klaus_cec_data::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::klaus_cec_data*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *klaus_cec_data::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_cec_data*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *klaus_cec_data::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::klaus_cec_data*)0x0)->GetClass(); }
   return fgIsA;
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_CalibreADC(void *p) {
      return  p ? new(p) ::CalibreADC : new ::CalibreADC;
   }
   static void *newArray_CalibreADC(Long_t nElements, void *p) {
      return p ? new(p) ::CalibreADC[nElements] : new ::CalibreADC[nElements];
   }
   // Wrapper around operator delete
   static void delete_CalibreADC(void *p) {
      delete ((::CalibreADC*)p);
   }
   static void deleteArray_CalibreADC(void *p) {
      delete [] ((::CalibreADC*)p);
   }
   static void destruct_CalibreADC(void *p) {
      typedef ::CalibreADC current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CalibreADC

namespace ROOT {
   // Wrappers around operator new
   static void *new_spsGainExtractor(void *p) {
      return  p ? new(p) ::spsGainExtractor : new ::spsGainExtractor;
   }
   static void *newArray_spsGainExtractor(Long_t nElements, void *p) {
      return p ? new(p) ::spsGainExtractor[nElements] : new ::spsGainExtractor[nElements];
   }
   // Wrapper around operator delete
   static void delete_spsGainExtractor(void *p) {
      delete ((::spsGainExtractor*)p);
   }
   static void deleteArray_spsGainExtractor(void *p) {
      delete [] ((::spsGainExtractor*)p);
   }
   static void destruct_spsGainExtractor(void *p) {
      typedef ::spsGainExtractor current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::spsGainExtractor

//______________________________________________________________________________
void klaus_event::Streamer(TBuffer &R__b)
{
   // Stream an object of class klaus_event.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(klaus_event::Class(),this);
   } else {
      R__b.WriteClassBuffer(klaus_event::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_klaus_event(void *p) {
      return  p ? new(p) ::klaus_event : new ::klaus_event;
   }
   static void *newArray_klaus_event(Long_t nElements, void *p) {
      return p ? new(p) ::klaus_event[nElements] : new ::klaus_event[nElements];
   }
   // Wrapper around operator delete
   static void delete_klaus_event(void *p) {
      delete ((::klaus_event*)p);
   }
   static void deleteArray_klaus_event(void *p) {
      delete [] ((::klaus_event*)p);
   }
   static void destruct_klaus_event(void *p) {
      typedef ::klaus_event current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::klaus_event

//______________________________________________________________________________
void klaus_acquisition::Streamer(TBuffer &R__b)
{
   // Stream an object of class klaus_acquisition.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(klaus_acquisition::Class(),this);
   } else {
      R__b.WriteClassBuffer(klaus_acquisition::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_klaus_acquisition(void *p) {
      return  p ? new(p) ::klaus_acquisition : new ::klaus_acquisition;
   }
   static void *newArray_klaus_acquisition(Long_t nElements, void *p) {
      return p ? new(p) ::klaus_acquisition[nElements] : new ::klaus_acquisition[nElements];
   }
   // Wrapper around operator delete
   static void delete_klaus_acquisition(void *p) {
      delete ((::klaus_acquisition*)p);
   }
   static void deleteArray_klaus_acquisition(void *p) {
      delete [] ((::klaus_acquisition*)p);
   }
   static void destruct_klaus_acquisition(void *p) {
      typedef ::klaus_acquisition current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::klaus_acquisition

//______________________________________________________________________________
void klaus_cec_data::Streamer(TBuffer &R__b)
{
   // Stream an object of class klaus_cec_data.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(klaus_cec_data::Class(),this);
   } else {
      R__b.WriteClassBuffer(klaus_cec_data::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_klaus_cec_data(void *p) {
      return  p ? new(p) ::klaus_cec_data : new ::klaus_cec_data;
   }
   static void *newArray_klaus_cec_data(Long_t nElements, void *p) {
      return p ? new(p) ::klaus_cec_data[nElements] : new ::klaus_cec_data[nElements];
   }
   // Wrapper around operator delete
   static void delete_klaus_cec_data(void *p) {
      delete ((::klaus_cec_data*)p);
   }
   static void deleteArray_klaus_cec_data(void *p) {
      delete [] ((::klaus_cec_data*)p);
   }
   static void destruct_klaus_cec_data(void *p) {
      typedef ::klaus_cec_data current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::klaus_cec_data

namespace ROOT {
   static TClass *vectorlETH1FmUgR_Dictionary();
   static void vectorlETH1FmUgR_TClassManip(TClass*);
   static void *new_vectorlETH1FmUgR(void *p = 0);
   static void *newArray_vectorlETH1FmUgR(Long_t size, void *p);
   static void delete_vectorlETH1FmUgR(void *p);
   static void deleteArray_vectorlETH1FmUgR(void *p);
   static void destruct_vectorlETH1FmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TH1F*>*)
   {
      vector<TH1F*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TH1F*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TH1F*>", -2, "vector", 214,
                  typeid(vector<TH1F*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETH1FmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TH1F*>) );
      instance.SetNew(&new_vectorlETH1FmUgR);
      instance.SetNewArray(&newArray_vectorlETH1FmUgR);
      instance.SetDelete(&delete_vectorlETH1FmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETH1FmUgR);
      instance.SetDestructor(&destruct_vectorlETH1FmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TH1F*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TH1F*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETH1FmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TH1F*>*)0x0)->GetClass();
      vectorlETH1FmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETH1FmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETH1FmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TH1F*> : new vector<TH1F*>;
   }
   static void *newArray_vectorlETH1FmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TH1F*>[nElements] : new vector<TH1F*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETH1FmUgR(void *p) {
      delete ((vector<TH1F*>*)p);
   }
   static void deleteArray_vectorlETH1FmUgR(void *p) {
      delete [] ((vector<TH1F*>*)p);
   }
   static void destruct_vectorlETH1FmUgR(void *p) {
      typedef vector<TH1F*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TH1F*>

namespace ROOT {
   static TClass *maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_Dictionary();
   static void maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_TClassManip(TClass*);
   static void *new_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p = 0);
   static void *newArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(Long_t size, void *p);
   static void delete_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p);
   static void deleteArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p);
   static void destruct_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<unsigned char,list<klaus_event> >*)
   {
      map<unsigned char,list<klaus_event> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<unsigned char,list<klaus_event> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<unsigned char,list<klaus_event> >", -2, "map", 96,
                  typeid(map<unsigned char,list<klaus_event> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<unsigned char,list<klaus_event> >) );
      instance.SetNew(&new_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR);
      instance.SetNewArray(&newArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR);
      instance.SetDelete(&delete_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR);
      instance.SetDestructor(&destruct_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<unsigned char,list<klaus_event> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<unsigned char,list<klaus_event> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<unsigned char,list<klaus_event> >*)0x0)->GetClass();
      maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEunsignedsPcharcOlistlEklaus_eventgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned char,list<klaus_event> > : new map<unsigned char,list<klaus_event> >;
   }
   static void *newArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned char,list<klaus_event> >[nElements] : new map<unsigned char,list<klaus_event> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p) {
      delete ((map<unsigned char,list<klaus_event> >*)p);
   }
   static void deleteArray_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p) {
      delete [] ((map<unsigned char,list<klaus_event> >*)p);
   }
   static void destruct_maplEunsignedsPcharcOlistlEklaus_eventgRsPgR(void *p) {
      typedef map<unsigned char,list<klaus_event> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<unsigned char,list<klaus_event> >

namespace ROOT {
   static TClass *listlEklaus_eventgR_Dictionary();
   static void listlEklaus_eventgR_TClassManip(TClass*);
   static void *new_listlEklaus_eventgR(void *p = 0);
   static void *newArray_listlEklaus_eventgR(Long_t size, void *p);
   static void delete_listlEklaus_eventgR(void *p);
   static void deleteArray_listlEklaus_eventgR(void *p);
   static void destruct_listlEklaus_eventgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const list<klaus_event>*)
   {
      list<klaus_event> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(list<klaus_event>));
      static ::ROOT::TGenericClassInfo 
         instance("list<klaus_event>", -2, "list", 503,
                  typeid(list<klaus_event>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &listlEklaus_eventgR_Dictionary, isa_proxy, 0,
                  sizeof(list<klaus_event>) );
      instance.SetNew(&new_listlEklaus_eventgR);
      instance.SetNewArray(&newArray_listlEklaus_eventgR);
      instance.SetDelete(&delete_listlEklaus_eventgR);
      instance.SetDeleteArray(&deleteArray_listlEklaus_eventgR);
      instance.SetDestructor(&destruct_listlEklaus_eventgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< list<klaus_event> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const list<klaus_event>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *listlEklaus_eventgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const list<klaus_event>*)0x0)->GetClass();
      listlEklaus_eventgR_TClassManip(theClass);
   return theClass;
   }

   static void listlEklaus_eventgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_listlEklaus_eventgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) list<klaus_event> : new list<klaus_event>;
   }
   static void *newArray_listlEklaus_eventgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) list<klaus_event>[nElements] : new list<klaus_event>[nElements];
   }
   // Wrapper around operator delete
   static void delete_listlEklaus_eventgR(void *p) {
      delete ((list<klaus_event>*)p);
   }
   static void deleteArray_listlEklaus_eventgR(void *p) {
      delete [] ((list<klaus_event>*)p);
   }
   static void destruct_listlEklaus_eventgR(void *p) {
      typedef list<klaus_event> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class list<klaus_event>

namespace {
  void TriggerDictionaryInitialization_runbinDict_Impl() {
    static const char* headers[] = {
"../../common//include/CalibreADC.h",
"../../common//include/spsGainExtractor.h",
"../../common//include/klaus_constants.h",
"../../common//include/EventType.h",
0
    };
    static const char* includePaths[] = {
"/usr/local/include/root",
"/usr/local/include/root",
"/home/user/git-repos/klaus-calibration/sweeps/LEDrun/runbin/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "runbinDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$../../common//include/CalibreADC.h")))  CalibreADC;
class __attribute__((annotate("$clingAutoload$../../common//include/spsGainExtractor.h")))  spsGainExtractor;
class __attribute__((annotate("$clingAutoload$../../common//include/EventType.h")))  klaus_event;
class __attribute__((annotate("$clingAutoload$../../common//include/EventType.h")))  klaus_acquisition;
class __attribute__((annotate("$clingAutoload$../../common//include/EventType.h")))  klaus_cec_data;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "runbinDict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "../../common//include/CalibreADC.h"
#include "../../common//include/spsGainExtractor.h"
#include "../../common//include/klaus_constants.h"
#include "../../common//include/EventType.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CalibreADC", payloadCode, "@",
"klaus_acquisition", payloadCode, "@",
"klaus_cec_data", payloadCode, "@",
"klaus_event", payloadCode, "@",
"spsGainExtractor", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("runbinDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_runbinDict_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_runbinDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_runbinDict() {
  TriggerDictionaryInitialization_runbinDict_Impl();
}
