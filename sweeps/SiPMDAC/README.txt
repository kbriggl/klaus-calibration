

################# Instructions for the use of this directory ####################

In this directory the scan and analysis scripts for a SiPM gain uniformization are given. They commonly use SPS spectra aquired with a LED or pulsed laser source.

The scan modifies the SiPM DAC of each given channel and takes spectra for a certain time. This has to adapted for the readout speed of the corresponding setup.
Adjust global threshold accordingly with the gui to get good spectra.

The analysis takes these spectra and extracts the gain with a FFT analysis "spsGainextractor" in ../common/src and perfroms a linear fit to the data. 
Remember: In regions of very low or very high DAC settings the gain can show non-linearities. The script also is able to set a common value of gain for all channels and writes a corresponding configuration
file. It also checks the available range of gain [ADC bins] and returns an error, if one of te channels can not be set to the given value. The usage of the script is explained at the beginning of the script.

Last change: E. Warttmann, 02.02.2022

