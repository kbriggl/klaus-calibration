

################ Instructions on the Hold DAC optimization scripts ##################

This directory allows the user to optimize the Hold DAC of the KLauS ASIC globally and channel-wise. 
Therefore a scan script is in /runbin. In the top structs the scanning range can be chosen. after each change this has to be compiled. 
Propably the scanning time has too be adjusted to the corresponding readout speed of the setup.

The analysis is done with a ROOT script. The instruction for execution are given in the top of the script itself. It uses a two-step optimization to first fix a global DAC, where the most channels feature a maximum value of 
digitized charge. Afterwards thie fine DAC gets adjusted by searching an individual maximum for each channel within this global setting. 

TODO: write optimized config file!

Last change: E. Warttmann, 01.02.2022

