

############### General Notes on the usage of this directory #######################

This directory contains all software to start necessary calibration measurements and analyse them in order to use them later in further measurements.
To use the executables, the KLauS-DAQ git repository in the KLauS6b branch is needed at the moment (TODO!)

The following list contains the calibration steps:

1. ADC linearity
	# Measure CDT, DNL, INL of each channel of the chip
	# USe output for further analysis (calibration database in form of a CalibBox class instance, saved in a root file)
2. Hold DAC
	# Optimization of global and channel-wise hold DAC with LED or testbeam data
	# Output: configuration with the optimal setting
3. SiPM bias
	# Gain Uniformization between channels
	# Needs SPS to evaluate gain in depndence of the DAC
	# Output: confiuration with uniformized settings
4. HV&Threshold scans (Mainly for timming measurements with K6b)
	# Find optimal working point for combination of tiles, SiPM and KLauS

Furthermore this directory contains multiple scripts for further calibration and measurements of the chip like PLL parameter, CEC scans, ... and a subdirectory "common" where all necessary classes are implemented to ensure the
funcionality of the scipts.  


Last change: E. Warttmann, 01.02.2022
