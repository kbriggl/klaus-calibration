


####### Introduction to analysis of high statistics testbeam runs for timing ######################

No scan routine. Uses TreeDAQ output.

Analysis:

GetRAWdiff: timing analysis for TDC/ADC uncorrected data.

GetCalibrationBox: Fills hits to calibrationBox.

getTWfits: includes CalibrationBox, perfoms DNL correction, fills TW histograms, fits and saves them. To reduce analysis time for later scripts.

getTDiffSiPMtypes: inlcudes CalibraionBox and TW histos. Performs DNL corrections and TW fitting, analyses different channel pairs on timing performance after TW correction.

getTDiffEnergyresolved: Same steps as in SiPMtypes, plus Energy resolved analyis of timing with fitting. Used with absorber data.

Last Change: E. Warttmann, 08.02.2022
 
