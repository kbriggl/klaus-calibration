

################# README to the TDC offset directory ##################

This directory provides the scan and analysis to calibrate the TDC offset for KLaus6b. 
this calibration is explicitly necessary for timing measurements and needs to be done in a testbeam environment.

The scan executable takes data with all possible fine counter offsets. TODO: include medium counter offset.
The behviour can be checked with the gui. 

The analysis creates time differences without TDC DNL correction and measures the RMS of the spectrum at a certain offset setting. Afterwards it compares the RMS of all settings. It responses with the 
setting with the lowest RMS. This setting should be used for all further timing measurements. 

It is important to check threshold settings beforehand. The electronic noise and DCR should not be visible in the GUI for either of the channels used in a pair.

Last change: E. Warttmann, 02.02.2022

