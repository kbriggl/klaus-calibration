#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "klaus_config_helpers.h"
#include "Agilent33250a.h"
#include "Agilent6614c.h"
#include "Keithley2200.h"
//#include "Keithley6487.h"
#include "HistogramDAQ_client.h"
#include "HistogrammedResults.h"
#include "Sequencer.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TObjString.h"
#include <signal.h>
// **************************
// Sweep options and propagated objects
struct{
	// **************************
	// Basic sweep params
	struct{
		std::list<unsigned short> TDCoffset_f=Seq::RangeSize<unsigned short>(0,31,1);
 	} opt;
} sweep;

#include "Setup.h"
#include "ADCMeasurement.h"

//SIGINT handler
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}


int main (int argc, char *argv[]){
	if (argc<4){
		std::cerr << "Usage: scan <host> <configbasename> <outputfile>\n";
		return -1;
	}

	//Init signal handler
	signal(SIGINT,&handler_sigint);

	//Init setup device tree
	setup.dev.SetN(1);

	//Init interface board
	setup.dev.config_ib=new VCRemoteClient<TIBConfig>(argv[1],0);
	setup.dev.config_ib->SetParValue("system/clkenable",1);
	setup.dev.config_ib->SetParValue("system/aqu_ena",1);
	setup.dev.config_ib->ChipReset();

	//Init ASIC configuration & DAQ
	char filename[256];
	for (int i=0;i<setup.dev.config_asic.size();i++){
		setup.dev.config_asic[i]=new VCRemoteClient<TKLauS4Config>(argv[1],1+i);
		sprintf(filename,"%s%d.txt",argv[2],i);
		printf("Reading file %s for asic %d\n",filename,i);
		setup.dev.config_asic[i]->ReadFromFile(filename);

		setup.dev.config_asic[i]->UpdateConfig();
		setup.dev.daq_histos[i]=new HistogramDAQ(argv[1],9090);
		//setup.dev.config_asic[i]->GetParValueWR("digital/i2c_address",addr);
		//setup.dev.daq_hist[i]->BindTo(addr);
		setup.dev.daq_hits=new EventListDAQ(argv[1],9090);
		setup.dev.daq_hits->RegisterQueue(5000,1);
	}
	Seq::Settling::Request(1000000);

	setup.dev.config_ib->SetParValue("system/led_system",0); //FIXME: LED is off

	//Init ROOT
	TFile fout(TString::Format("%s",argv[3]),"recreate");

	printf("Reading Temperatures\n");	
	fout.cd();
	TObjString s(setup.dev.daq_histos[0]->ReadTemperatures().c_str());
	cout <<"Temperatures:" << setup.dev.daq_histos[0]->ReadTemperatures().c_str() << endl;
	s.Write();
	
	setup.trees.push_back(new TTree("tr_histos","histograms"));
	setup.trees.push_back(new TTree("tr_hits","acquisition"));
	for (auto t: setup.trees)
		t->AutoSave();

	Seq::MeasurementCollection todo;
	todo.Add("led-run",new Measurement(setup.trees[0],setup.trees[1],60e3));

	//Fix to HG mode
	for (auto config: setup.dev.config_asic){
		config->SetParValue("digital/TDC_offset_enable",1);
		//SetParAllChannel(config,"HG_scale_select",1);	//using mixed configuration based on input file
		//SetParAllChannel(config,"branch_sel_config",3);	//FORCE HG
	}
	//Save the configuration state
	SaveConfigInROOT(setup.dev.config_asic[0],"config_ASIC");
	SaveConfigInROOT(setup.dev.config_ib,"config_IB");
	SaveConfigStatusInROOT(setup.dev.config_ib,"status_IB");

	Seq::Settling::Settle();

	Seq::Sequence<unsigned short>(setup.trees,"TDCoffset_f","TDCoffset_f/s").LoopOver(sweep.opt.TDCoffset_f,
			[&todo](Seq::Sequence<unsigned short>* this_seq){
			std::cout<<" TDC offset fine: "<<this_seq->GetObservable()<<std::endl;
			for (auto config: setup.dev.config_asic){
				config->SetParValue("digital/TDC_offset_fine",this_seq->GetObservable());	
				config->UpdateConfig();
				config->UpdateConfig();
				Seq::Settling::Request(10000);
				}
				//Measurements
				todo.Loop([this_seq](Seq::MeasurementBase* job){
				job->Run();
				return true;
			});//measurements
		return true;
	});//Hold DAC global
	//Disable LED system
	setup.dev.config_ib->SetParValue("system/led_system",0);
//	setup.dev.LED_supply->SetVolt(0);

        printf("Finish the scan\n");
	fout.cd();
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	fout.Close();
	return 0;
}
