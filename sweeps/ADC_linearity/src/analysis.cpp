#include <list>
#include <map>
#include <algorithm>
#include <iostream>
#include <vector>
#include <set>

#include "TTree.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TEfficiency.h"
#include "HistogrammedResults.h"
#include "EventType.h"
#include <stdio.h>
#define	ASNI_COLOR_RED			"\x1b[31m"
#define	ASNI_COLOR_GREEN		"\x1b[32m"
#define	ASNI_COLOR_BLUE			"\x1b[34m"
#define	ASNI_COLOR_RESET		"\x1b[0m"

using namespace std;

#include "AnalyzeADC.h"

int main(int argc, char* argv[]){
	if(argc<2){
		printf("Usage: analysis <input file name>\n");
		return -1;
	}

	char ifname[256], ofname[256];
	sprintf(ifname,"%s",argv[1]); 
	sprintf(ofname,"analysed-%s",argv[1]); 

	TFile* file = new TFile(argv[1]);
	TTree* tree = (TTree*)file->Get("cdt");
	unsigned short channel;
	tree->SetBranchAddress("channel",&channel);
	HistogrammedResults* res = NULL;
	tree->SetBranchAddress("histos_chip255",&res);


	// raw data histogram
	vector<int> channelList;
	for(int i=0;i<tree->GetEntries();i++){
		tree->GetEntry(i);
		channelList.push_back(channel);
	}
	std::set<int> s1(channelList.begin(),channelList.end()); channelList.assign(s1.begin(),s1.end());

	printf("scanned %d channels found\n ",channelList.size());

/* ----------------------------------------------------------------------------------- 	*/
/*			Prepare all the plots						*/
/* ----------------------------------------------------------------------------------- 	*/

	// 2-D histograms to store all the orignial results
	vector<TH1F*> histos;	// orginal histogram
	for(unsigned int i=0;i<channelList.size();i++){
		TH1F* histo = new TH1F(	Form("CH%d",channelList[i]), Form("CH%d",channelList[i]),1024,0,1024);
		histos.push_back(histo);
	}
	vector<TH1F*> norms;	// normalized histogram
	for(unsigned int i=0;i<channelList.size();i++){
		TH1F* histo = new TH1F(	Form("Norm_CH%d",channelList[i]), Form("Norm_CH%d",channelList[i]),1024,0,1024);
		norms.push_back(histo);
	}
	vector<TH1F*> dnls;	// DNL
	for(unsigned int i=0;i<channelList.size();i++){
		TH1F* histo = new TH1F(	Form("DNL_CH%d",channelList[i]), Form("DNL_CH%d",channelList[i]),1024,0,1024);
		dnls.push_back(histo);
	}
	vector<TH1F*> inls;	// histogram
	for(unsigned int i=0;i<channelList.size();i++){
		TH1F* histo = new TH1F(	Form("INL_CH%d",channelList[i]), Form("INL_CH%d",channelList[i]),1024,0,1024);
		inls.push_back(histo);
	}
	TGraph* dnl_min = new TGraph(); dnl_min->SetNameTitle("DNL_Min","DNL_Min");
	TGraph* dnl_max = new TGraph(); dnl_max->SetNameTitle("DNL_Max","DNL_Max");
	TGraph* inl_min = new TGraph(); inl_min->SetNameTitle("INL_Min","INL_Min");
	TGraph* inl_max = new TGraph(); inl_max->SetNameTitle("INL_Max","INL_Max");


/* ----------------------------------------------------------------------------------- 	*/
/*			read data and analysis						*/
/* ----------------------------------------------------------------------------------- 	*/

	// read the data into the histograms and analysis
	std::vector<int>::iterator iit;
	int channelIndex;
	for(int entry=0;entry<tree->GetEntries();entry++){
		tree->GetEntry(entry);
		// find the exact index to store the data
		iit = std::find(channelList.begin(),channelList.end(),channel);
		channelIndex = std::distance(channelList.begin(), iit);

		histos[channelIndex]->Add(res->h_ADC_10b[channel]);
	}
	printf("successfully readout the data\n");

	// ADC nonlnearioty analysis
	ADCLinearity* analyzer = new ADCLinearity(10);
	int xmin = 680; int xmax = 1022;
	double minDNL, maxDNL, minINL, maxINL;
	for(unsigned int i=0;i<channelList.size();i++){
		analyzer->AnalysisHistoNonLinearity(histos[i],xmin,xmax);
		analyzer->GetDNL(dnls[i],minDNL, maxDNL);
		analyzer->GetINL(inls[i],minINL, maxINL);
		analyzer->GetNorm(norms[i]);
		dnl_min->SetPoint(dnl_min->GetN(),channelList[i],minDNL);
		dnl_max->SetPoint(dnl_max->GetN(),channelList[i],maxDNL);
		inl_min->SetPoint(inl_min->GetN(),channelList[i],minINL);
		inl_max->SetPoint(inl_max->GetN(),channelList[i],maxINL);
	}
	printf("successfully analysis the data\n");
	
/* ----------------------------------------------------------------------------------- 	*/
/*			save the result							*/
/* ----------------------------------------------------------------------------------- 	*/
	TFile* ofile = new TFile(ofname,"recreate");
	TDirectory* raw = ofile->mkdir("raw"); raw->cd();
	for(unsigned int i=0;i<channelList.size();i++){ histos[i]->Write(); }
	TDirectory* norm = ofile->mkdir("norm"); norm->cd();
	for(unsigned int i=0;i<channelList.size();i++){ norms[i]->Write(); }
	TDirectory* fdnl = ofile->mkdir("DNL"); fdnl->cd();
	for(unsigned int i=0;i<channelList.size();i++){ dnls[i]->Write(); }
	TDirectory* finl = ofile->mkdir("INL"); finl->cd();
	for(unsigned int i=0;i<channelList.size();i++){ inls[i]->Write(); }
	TDirectory* minmax = ofile->mkdir("minmax"); minmax->cd();
	dnl_min->Write(); dnl_max->Write(); inl_min->Write(); inl_max->Write();

	ofile->Close();
	file->Close();
}
