#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "config_helpers.h"



#include "HistogramDAQ_client.h"
#include "HistogrammedResults.h"
#include "Sequencer.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

//#include "Agilent33250a.h"
//#include "Keithley6487.h"
//#include "SCPIoUART.h"

#include <string>
#include <stdlib.h>
#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <iterator>
using namespace std;

#define C_GREEN "\x1B[32m"
#define C_RESET "\x1B[0m"

#include <ctime>
#include <signal.h>

// **************************
// Sweep options and propagated objects
struct{
	// **************************
	// Basic sweep params
	struct{
		//std::list<unsigned short> boardIDs=Seq::RangeList<unsigned short>("1");
		std::list<unsigned short> adcmodes=Seq::RangeList<unsigned short>("0"); //  0(SAR) 1(PIPE)
		std::list<unsigned short> channels=Seq::RangeSize<unsigned short>(0,35,1);
		//std::list<unsigned short> channels=Seq::RangeList<unsigned short>("0");
		//std::list<unsigned short> capas=Seq::RangeList<unsigned short>("33");
 	} opt;
} sweep;

#include "Setup.h"
#include "ADCMeasurement.h"

//SIGINT handler
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}


int main (int argc, char *argv[]){
	if (argc<4){
		std::cerr << "Usage: scan <host> <configbasename> <outputfile>\n";
		return -1;
	}

	//Init signal handler
	signal(SIGINT,&handler_sigint);

	//Init setup device tree
	setup.dev.SetN(1);

	//Init interface board
	setup.dev.config_ib=new VCRemoteClient<TIBConfig>(argv[1],0);
	setup.dev.config_ib->SetParValue("system/clkenable",1);
	setup.dev.config_ib->SetParValue("system/aqu_ena",1);
	setup.dev.config_ib->ChipReset();

	//Init ASIC configuration & DAQ
	char filename[256];
	for (int i=0;i<setup.dev.config_asic.size();i++){
		setup.dev.config_asic[i]=new VCRemoteClient<TKLauS4Config>(argv[1],1+i);
		sprintf(filename,"%s%d.txt",argv[2],i);
		printf("Reading file %s for asic %d\n",filename,i);
		setup.dev.config_asic[i]->ReadFromFile(filename);

		setup.dev.config_asic[i]->UpdateConfig();
		setup.dev.daq_histos[i]=new HistogramDAQ(argv[1],9090);
		//setup.dev.config_asic[i]->GetParValueWR("digital/i2c_address",addr);
		//setup.dev.daq_hist[i]->BindTo(addr);
		setup.dev.daq_hits=new EventListDAQ(argv[1],9090);
		setup.dev.daq_hits->RegisterQueue(5000,1);
	}
	Seq::Settling::Request(1000000);



        //Init Pulsegenerator
/*	setup.dev.pulsegen=new Agilent33250a("/dev/ttyS0");
	static_cast<Agilent33250a*>(setup.dev.pulsegen)->Ramp();
	static_cast<Agilent33250a*>(setup.dev.pulsegen)->SetPeriod(9e-4);
	static_cast<Agilent33250a*>(setup.dev.pulsegen)->SetHiLevel(1.65);
	static_cast<Agilent33250a*>(setup.dev.pulsegen)->SetLoLevel(0.4);
	static_cast<Agilent33250a*>(setup.dev.pulsegen)->SetSymmetry(50);	
	setup.dev.pulsegen->On();
	Seq::Settling::Request(1000000);
*/
	//Init ROOT
	TFile fout(TString::Format("sar-%s",argv[3]),"recreate");
	setup.trees.push_back(new TTree("cdt","cdt"));
	setup.trees.push_back(new TTree("dump","acquisition"));
	for (auto t: setup.trees)
		t->AutoSave();
	
	Seq::MeasurementCollection todo;
//	todo.Add("cdt",new Measurement(setup.trees[0],setup.trees[1],875e3));
	todo.Add("cdt",new Measurement(setup.trees[0],setup.trees[1],150e3)); 

	// Some configuration settings
	SetParAllChannel(setup.dev.config_asic[0],"ADC_in_n_select",1); 	// inner ground
	SetParAllChannel(setup.dev.config_asic[0],"ext_trigger_select",0); 	// external trigger
	SetParAllChannel(setup.dev.config_asic[0],"trigger_th_finetune",0);	// trigger finetune DAC=0
	SetParAllChannel(setup.dev.config_asic[0],"trigger_LSB_scale",0);	// trigger finetune LSB no scale
	setup.dev.config_asic[0]->SetParValue("comparator_th_trig",0);		// set the trigger threshold to be zero

	//Save the configuration state
	SaveConfigInROOT(setup.dev.config_asic[0],"config_asic");
	SaveConfigInROOT(setup.dev.config_ib,"config_IB");
	SaveConfigStatusInROOT(setup.dev.config_ib,"status_IB");

	Seq::Settling::Settle();

	Seq::Sequence<unsigned short>(setup.trees,"channel","channel/s").LoopOver(sweep.opt.channels,
	[&todo](Seq::Sequence<unsigned short>* this_seq){
		unsigned short channel=this_seq->GetObservable();
		printf("Channel: %u\n",this_seq->GetObservable());
		SetParAllChannel(setup.dev.config_asic[0],"mask",1);
		SetParForChannel(setup.dev.config_asic[0],channel,"mask",0);
		SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",0); 
		SetParForChannel(setup.dev.config_asic[0],channel,"branch_sel_config",1);
		Seq::Sequence<unsigned short>(setup.trees,"adc_mode","adc_mode/s").LoopOver(sweep.opt.adcmodes,
		[&todo](Seq::Sequence<unsigned short>* this_seq){
			printf("ADC_MODE: %s\n",this_seq->GetObservable()==0?"10b":"12b");
			setup.dev.config_asic[0]->SetParValue("digital/pipemode_enable",this_seq->GetObservable());
		
			//Separate measurements
			todo.Loop([this_seq](Seq::MeasurementBase* job){
				setup.dev.config_asic[0]->UpdateConfig();
				setup.dev.config_asic[0]->UpdateConfig();
				job->Run();
			});//measurements

			return true;
		});//ADC mode
		return true;
	});//Channel

        printf("Finish the scan\n");
	fout.cd();
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	fout.Close();
	return 0;
}
