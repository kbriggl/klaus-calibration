

########### README for ADC linearity scan and analysis ################

This folder contains a scan and analysis script to evaluate the ADC Non-Linearities of the KLauS ASIC

Difficulties with building this directory can be overcome with an adaption of the libana.common build in ../common. There is a description given in the README and code.

To get a valid result, the external input to the ADC on the testboard (ADCp) has to be used. There a arbitrary waveform generator with a ramp signal needs to be connected.
As an orientation the amplitude range of this ramp signal is about 250-800 mV at a frequency at a few kHz.

In the configuration the branch selection of the channel has to be set to "external". This is also done by the scan script. 

It is useful to check the signal output with the gui before the scan is started. The data taking can be monitored with the gui during the script is running. 
The duration of the scan is dependent on the readout speed of the RaspberryPi. Each ADC bin should get about 10000 events, to keep the statistical error below 1%.

The analysis script creates a file, which can be used in most of the following calibration measurements and analysises. Therefore a tool called the "CalibrationBox" is created. 
A description of this tool is given in the corresponding README file of the analysis you will use. 
This file can be inspected with the standard ROOT TBrowser. 


Last change: E. Warttmann, 18.02.2022

