
//DAQ
#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "config_helpers.h"
#include "HistogramDAQ_client.h"
#include "HistogrammedResults.h"
#include "Sequencer.h"

//device library
//#include "Agilent33250a.h"
//#include "Agilent6614c.h"
//#include "Keithley2200.h"
//#include "Keithley6487.h"

//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include <signal.h>
// **************************
// Sweep options and propagated objects
struct{
	// **************************
	// Basic sweep params
	struct{
		std::list<char> gbranch_sel	= Seq::RangeList<char>("H M L h m"); // HG, MG, LG, AG(HG,LG), AG(MG,LG)
 	} opt;
} sweep;
#include "Setup.h"
#include "ADCMeasurement.h"

//SIGINT handler
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}


int main (int argc, char *argv[]){
	if (argc<4){
		std::cerr << "Usage: scan <host> <configbasename> <outputfile>\n";
		return -1;
	}

	//Init signal handler
	signal(SIGINT,&handler_sigint);

	//Init setup device tree
	setup.dev.SetN(1);

	//Init interface board
	setup.dev.config_ib=new VCRemoteClient<TIBConfig>(argv[1],0);
	setup.dev.config_ib->SetParValue("system/clkenable",1);
	setup.dev.config_ib->SetParValue("system/aqu_ena",1);
	setup.dev.config_ib->ChipReset();

	//Init ASIC configuration & DAQ
	char filename[256];
	for (int i=0;i<setup.dev.config_asic.size();i++){
		setup.dev.config_asic[i]=new VCRemoteClient<TKLauS4Config>(argv[1],1+i);
		sprintf(filename,"%s%d.txt",argv[2],i);
		printf("Reading file %s for asic %d\n",filename,i);
		setup.dev.config_asic[i]->ReadFromFile(filename);

		setup.dev.config_asic[i]->UpdateConfig();
		setup.dev.daq_histos[i]=new HistogramDAQ(argv[1],9090);
		//setup.dev.config_asic[i]->GetParValueWR("digital/i2c_address",addr);
		//setup.dev.daq_hist[i]->BindTo(addr);
		setup.dev.daq_hits=new EventListDAQ(argv[1],9090);
		setup.dev.daq_hits->RegisterQueue(5000,1);
	}
	Seq::Settling::Request(1000000);

	////Init LED power supply
	//setup.dev.LED_supply = new Agilent6614c();
	//setup.dev.LED_supply->Setcom("/dev/ttyS0");
	//setup.dev.LED_supply->SetVolt(1);
	//setup.dev.LED_supply->On();
	setup.dev.config_ib->SetParValue("system/led_system",0);

	//Init ROOT
	TFile fout(TString::Format("%s",argv[3]),"recreate");


	printf("Reading Temperatures\n");	
	fout.cd();
	TObjString s(setup.dev.daq_histos[0]->ReadTemperatures().c_str());
	cout <<"Temperatures:" << setup.dev.daq_histos[0]->ReadTemperatures().c_str() << endl;
	s.Write();
	
	setup.trees.push_back(new TTree("tr_histos","histograms"));
	setup.trees.push_back(new TTree("tr_hits","acquisition"));
	for (auto t: setup.trees)
		t->AutoSave();

	Seq::MeasurementCollection todo;
	todo.Add("AG-run",new Measurement(setup.trees[0],setup.trees[1],30e3));
	//set all thresholds to zero  - this is a noise measurement!
	for (int i=0;i<setup.dev.config_asic.size();i++){
		setup.dev.config_asic[i]->SetParValue("bias/comparator_th_trig",0);
		setup.dev.config_asic[i]->UpdateConfig();
	}



	//Save the configuration state
	SaveConfigInROOT(setup.dev.config_asic[0],"config_ASIC");
	SaveConfigInROOT(setup.dev.config_ib,"config_IB");
	SaveConfigStatusInROOT(setup.dev.config_ib,"status_IB");

	Seq::Settling::Settle();

	Seq::Sequence<char>(setup.trees,"gbranch_sel","gbranch_sel/B").LoopOver(sweep.opt.gbranch_sel,
	[&todo](Seq::Sequence<char>* this_seq){
		SetParAllChannel(setup.dev.config_asic[0],"LG_scale_select",1);
		switch(this_seq->GetObservable()){
			case 'H':	//FORCE HG
				SetParAllChannel(setup.dev.config_asic[0],"HG_scale_select",1);
				SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",3);
				std::cout<<" Gain Branch selection: HG force"<<std::endl;
				break;
			case 'M':	//FORCE MG
				SetParAllChannel(setup.dev.config_asic[0],"HG_scale_select",0);
				SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",3);
				std::cout<<" Gain Branch selection: MG force"<<std::endl;
				break;
			case 'L':	//FORCE LG
				SetParAllChannel(setup.dev.config_asic[0],"HG_scale_select",1);
				SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",2);
				std::cout<<" Gain Branch selection: LG force"<<std::endl;
				break;
			case 'h':	//Autogain HG / LG
				SetParAllChannel(setup.dev.config_asic[0],"HG_scale_select",1);
				SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",0);
				std::cout<<" Gain Branch selection: HG / LG autogain"<<std::endl;
				break;
			case 'm':	//Autogain MG / LG
				SetParAllChannel(setup.dev.config_asic[0],"HG_scale_select",1);
				SetParAllChannel(setup.dev.config_asic[0],"branch_sel_config",0);
				std::cout<<" Gain Branch selection: MG / LG autogain"<<std::endl;
				break;
			
			default:
				std::cout<<" Gain Branch selection - UNKNOWN setting"<<std::endl;
		}

		for (auto config: setup.dev.config_asic){
			config->UpdateConfig();
			config->UpdateConfig();
			Seq::Settling::Request(10000);
		}
		//Measurements
		todo.Loop([this_seq](Seq::MeasurementBase* job){
			job->Run();
			return true;
		});//measurements
		return true;
	});//gain select

	//Disable LED system
	setup.dev.config_ib->SetParValue("system/led_system",0);
	//setup.dev.LED_supply->SetVolt(0);

        printf("Finish the scan\n");
	fout.cd();
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	fout.Close();
	return 0;
}
