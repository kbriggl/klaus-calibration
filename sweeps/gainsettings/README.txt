

################ Description of this directory #####################

This directory provides the scan analysis scripts to evaluate the relative scaling between the different branches of digitization in the KLauS ASIC ADC. 
These information is important if data taking was done in a branch where the single photon peaks can not be resolved and a calibration of the data to photoelectron equivalent is desired.
Also the scaling factors are necessary if the data was taken in AutoGain mode and shall be reconstructed afterwards in the analysis. For this a feature in the CalibrationBox exists.

The scan executable takes data (Led, laser, better testbeam) in different branches and auto gain mode. The corresponding threshold needs to be set manually.
It is important to set the gain selection threshold to an appropriate vlaue.

The analysis then does efficiency curves of the data and then looks for the middle point of the S-curve and compares them with the one from other branches to evaluate the scaling factor. 
For this, the pedestal of the different branches of each channel needs to be known and put into the analysis. 

Comment: An easy way of estimating this by hand is to take the mean value of a SPS i.e. in different branches and calculate the scaling by: Scaling(HG-LG)=(Signal_HG-Pdeestal_HG)/(Signal_LG-Pedestal_LG)

The values has to be entered manually into the analysis at this point, if auto gain is used. (See CalibrationBox)

Last change: E. Warttmann, 02.02.2022

