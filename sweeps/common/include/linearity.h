#include "TF1.h"
#include "TH1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"

//generate dnl plot from graph and slope
TGraph GenDNL(TGraph* gr_in, double slope);

//generate inl plot from graph and function
TGraphErrors GenINL(TGraphErrors* gr_in, TF1 func);
//generate relative inl plot from graph and function
TGraphErrors GenINLrel(TGraphErrors* gr_in, TF1 func);

//generate linear fit of data in gr_in where the inl is less than inl_limit[%] FSR
TF1 inl_limit_fit(TGraphErrors* gr_in, double inl_limit,bool subrangecheck=false);

//Calculate binary delta errors from dnl plot
TGraphErrors Dnl2BitDelta(TGraph* g_dnl);
//Get relative errors
TGraphErrors BitDeltaRel(TGraphErrors& g_bitDelta);

//Generate INL from relative delta errors
TGraphErrors Delta2INL(TGraphErrors* g_delta);

//Compute difference of two inl plots
TGraph INLdiff(TGraph* g1, TGraph* g2);
TGraphErrors INLdiff(TGraph* g1, TGraphErrors* g2);
TGraphErrors INLdiff(TGraphErrors* g1, TGraphErrors* g2);

