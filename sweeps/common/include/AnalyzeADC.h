/*
    Here we defined a packets of class for ADC performance analysis
    1. ADCLinearity:    Non-Linearity analysis, such as DNL
    2. ADCDynamic:      Dynamic performance analysis, such as ENOB, SNDR, HD etc.
*/
#ifndef ANALYZEADC_H
#define ANALYZEADC_H

#include <iostream>
#include <vector>
#include "TH1F.h"
using namespace std;

// Analysis DNL/INL from CDT measurements
class 	ADCLinearity{
	public:
		// constructor
		ADCLinearity(int Nbit){m_Nbit=Nbit;}

		// basic non-linearity analysis method based on vector
		void 	AnalysisNonLinearity(std::vector<int>& CDT, int xmin, int xmax);
		std::vector<double> 	GetDNL(double& minDNL, double& maxDNL);
		std::vector<double> 	GetINL(double& minINL, double& maxINL);

		// non-linearity method based on histogram
		void	AnalysisHistoNonLinearity(TH1F* histo, int xmin, int xmax);
		void	GetDNL(TH1F* histo, double& minDNL, double& maxDNL);
		void	GetINL(TH1F* histo, double& minINL, double& maxINL);
		void	GetNorm(TH1F* histo);

	private:
		int 	m_Nbit; // for SAR=10; Pipe=13;
		double 	m_minDNL, m_maxDNL;
		double 	m_minINL, m_maxINL;
		std::vector<double> m_DNL;
		std::vector<double> m_INL;
};

// Dynamic performance analysis from a value~time measurements
class ADCDynamic{
        private:
                // datas for analysis
                vector<int>     m_ADC;          // data converted by the ADC
                int             m_NSample;      // number of sampling data in the array
                double          m_fs;           // sampling frequency

                // FFT analysis
                vector<double>  m_DataAfterWindow;
                vector<double>  m_fft;
                vector<double>  m_fftdBFS;              // vector for FFT power results
                double          m_SNR, m_SNDR, m_ENOB;
                double          m_SFDR;
                void            FFT1D(int windowType, double kaiserAlpha);
                void            AnalysisFFTResults();
                int             m_dcWindow;             // DC power window
                int             m_signalWindow;         // signal power window
                int             m_harmonicWindow;       // harmonic distoration power window
                int             m_harmonicToneNum;      // number of harmonic tone want to calculate
                
                // windowing function for FFT
                vector<double>  m_Hanning;
                void            GetHanningWindow(int flag=1); // default is "period"
                vector<double>  m_Blackman;
                void            GetBlackmanWindow();
		vector<double> 	m_Kaiser;
		void 		GetKaiserWindow(double alpha = 15.0);

                

        public:
                ADCDynamic(double fs=1.0);

                // general functions to set the basic parameters
                void            SetDataArrayForFFT(vector<int>& ADC, int NSample);
                void            SetNSample(int NSample);
                void            SetFFTParameters(int dcWindow=1, int signalWindow=1, 
                                                 int harmonicWindow=1, int harmonicToneNum=1);

                // FFT related results
                void            DynamicAnalysis(int flag=0, double kaiserAlpha=15.0);
                vector<double>  GetFFT();
                void            PrintFFTResults();

};

// Pipelined(two-stage) ADC code merger for the 6b+8b configuration
class 	PipeBranchMerger{
	private:
		std::vector<int> m_ADC6b; 	// single event array
		std::vector<int> m_ADC8b; 	// single event array
		std::vector<int> m_ADC14bCDT;

		int m_12bStart, m_12bEnd;
		std::vector<int> m_ADC12bCDT;  	// MOST IMPORTANT for DNL/INL analysis
		std::vector<int> m_Offset;

		// for pipeline reconstruction
		int     FindOffsetForNeighboorBranches(std::vector<int> &h1, std::vector<int> &h2);
		bool    FindEdgeOf8bCDTArray(std::vector<int> &histo, int& rEdge, int& fEdge);
		void 	GetMerged12bDataArray(); 	// generate the 12b merged CDT from m_ADC14bCDT;


	public:
		PipeBranchMerger();

		// Set Data Array
		void 	Set14bDataArray(std::vector<int>& ADC6b, std::vector<int>& ADC8b);      // Set the 14-bit data array
		void 	Set14bDataArray(std::vector<int>& ADC14bCDT){m_ADC14bCDT = ADC14bCDT;}  // directly set the data array

		// generate the Merged 12b data from 6b + 8b input
                // This is used when the merger process is done!
		int 	GetMerged12bCode(int adc6b, int adc8b){return adc8b+m_Offset[adc6b];}

		// Get the cdt for the DNL/INL analysis
		// xmin. xmax is the min/max code range for the analysis
		std::vector<int> Get12bCodeDensity(int& xmin, int& xmax){this->GetMerged12bDataArray();xmin=m_12bStart; xmax=m_12bEnd; return m_ADC12bCDT;}

};



#endif
