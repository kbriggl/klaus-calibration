#ifndef SPSGAINEXTRACTOR_H
#define SPSGAINEXTRACTOR_H

#include <list>
#include <algorithm>
#include <iostream>
#include <vector>
#include "TGraphErrors.h"
#include "TH1F.h"

using namespace std;

class   spsGainExtractor{
    public:
        TH1F*   sps;
        double  pedestal;
        double  gain_fft;
        double  gain_fit;
        double  gain_err;   // error from gain_fit
        double  threshold;

        // constructor
        spsGainExtractor(){};
        spsGainExtractor(double p){
            pedestal    =   p;
        };

        spsGainExtractor(TH1F* s, double p){
            sps         =   s;
            pedestal    =   p;
            gain_fft    =   -1;
            gain_fit    =   -1;
            gain_err    =   0;
        };
        
        // methods
        void    SetPedestalValue(double p){pedestal = p;};
        void    SetHistogram(TH1F* histo){sps = histo;};
        bool    ExtractGainByFFT(int fftmin=10, int fftmax=100);
	TGraph* ExtractGainByTSpectrum();
        //TGraphErrors* ExtractGainAndThreshold(int fftmin=10, int fftmax=100);

    private:
        void    spsSort(double* a, int n);
    
};

#endif
