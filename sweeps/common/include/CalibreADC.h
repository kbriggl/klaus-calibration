#ifndef CALIBREADC_H
#define CALIBREADC_H

#include <iostream>
#include <vector>
#include "TH1F.h"
#include "TFile.h"

// Provide the interface to read the normalized cdt histogram,
// and correct the spectrum under calibration
class   CalibreADC{
        private:
                std::vector<TH1F*> m_ncdt;      // normalized cdt histogram


        public:
                CalibreADC();
                CalibreADC(const char* ncdtfname);

                void    SetNormCDT(TH1F* cdt, int channel);           // set the m_ncdt[channel] explicity
                TH1F*   GetVariableBinsizedHistogram(TH1F* histo, int channel);
                TH1F*   GetCalibredHistogram(TH1F* histo, int channel); // calculate the calibration
};


#endif
