
std::list<float> GetRange(float V_range,float V_step, float V_start=1){
	if(V_step<.1){
		V_step=.1;
	}
	if(V_step<1 && V_start<1) V_start=1;

	std::cout<<"  Scale setting: New range: "<<V_start<<".."<<V_range<<"; Step="<<V_step<<std::endl;
	//round the step and ranges to make sure the pulse generator can handle the same precision
		int iV_start=(V_start*10);
		int iV_range=(V_range*10);
	int iV_step=(V_step*10);

	return Seq::RangeSize<float>(iV_start/10.,iV_range/10.,iV_step/10.);
}


//Limit function: larger than 1.0mV, smaller than 10V
bool badpoint (const float& value) { return (value<1.0) || (value>10000); }

std::list<float> SetScaleGetRange(short scale, float Cdet, int nsteps, bool amon_enable, unsigned short channel, float addfull_until=0, float HG_range=3000, float MG_range=18000, float LG_range=160000, float LLG_range=400000){
	std::list<float> vrange; //list of points to step through
	SetParAllChannel(setup.dev.asic,"monitor_HG_disable",1);
	SetParAllChannel(setup.dev.asic,"monitor_LG_disable",1);
	if(scale==-1){	//Auto gain 1:1 - 1:40
		SetParAllChannel(setup.dev.asic,"HG_scale_select",1);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",0);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.splice(vrange.end(),GetRange(HG_range/Cdet,HG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 3pC
		vrange.splice(vrange.end(),GetRange(LG_range/Cdet,LG_range/Cdet* 1./(nsteps-1.),HG_range/Cdet)); //expected range < 160pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else
	if(scale==-7){	//Auto gain 1:1 - 1:40
		SetParAllChannel(setup.dev.asic,"HG_scale_select",0);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",0);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.splice(vrange.end(),GetRange(MG_range/Cdet,MG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 18pC
		vrange.splice(vrange.end(),GetRange(LG_range/Cdet,LG_range/Cdet* 1./(nsteps-1.),MG_range/Cdet)); //expected range < 160pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else
	if(scale==1){	//Force 1:1
		SetParAllChannel(setup.dev.asic,"HG_scale_select",1);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",3);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.splice(vrange.end(),GetRange(HG_range/Cdet,HG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 3pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else
	if(scale==7){	//Force 1:7
		SetParAllChannel(setup.dev.asic,"HG_scale_select",0);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",3);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.splice(vrange.end(),GetRange(MG_range/Cdet,MG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 18pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else
	if(scale==48){	//Force 1:48
		SetParAllChannel(setup.dev.asic,"branch_sel_config",2);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_LG_disable",0);
		vrange.splice(vrange.end(),GetRange(LG_range/Cdet,LG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 160pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else
	if(scale==120){	//Force 1:120
		SetParAllChannel(setup.dev.asic,"branch_sel_config",2);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",1); 	// Large LG scale 1:48->0; 1:120->1
		if(Cdet<100){ // require the capacitor as least 100pF, this is reqiured by the design for large array SiPM
			printf("Error: wrong scale: %u\n",scale);
			std::list<float> a;
			return a;
		}
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_LG_disable",0);
		vrange.splice(vrange.end(),GetRange(LLG_range/Cdet,LLG_range/Cdet* 1./(nsteps-1.),1)); //expected range < 400pC
		if(addfull_until>1) Seq::RangeAddEps(vrange,Seq::RangeSize<float>(1,addfull_until,0.1));
	}else{
		printf("Error: wrong scale: %u\n",scale);
		std::list<float> a;
		return a;
	}
	
	//remove out-of range points
	vrange.remove_if(badpoint);
	return vrange;
}


std::list<float> SetScaleGetRangeForFixedHoldDelay(short scale, float Cdet, int nsteps, bool amon_enable, unsigned short channel, float addfull_until=0, float HG_range=3000, float MG_range=18000, float LG_range=160000, float LLG_range=400000){
	std::list<float> vrange; //list of points to step through
	SetParAllChannel(setup.dev.asic,"monitor_HG_disable",1);
	SetParAllChannel(setup.dev.asic,"monitor_LG_disable",1);
	if(scale==1){	//Force 1:1
		SetParAllChannel(setup.dev.asic,"HG_scale_select",1);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",3);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.push_back(0.071*33/Cdet);
	}else
	if(scale==7){	//Force 1:7
		SetParAllChannel(setup.dev.asic,"HG_scale_select",0);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		SetParAllChannel(setup.dev.asic,"branch_sel_config",3);
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_HG_disable",0);
		vrange.push_back(0.433*33/Cdet);
	}else
	if(scale==48){	//Force 1:48
		SetParAllChannel(setup.dev.asic,"branch_sel_config",2);
		SetParAllChannel(setup.dev.asic,"LG_scale_select",0); 	// Large LG scale 1:48->0; 1:120->1
		if(amon_enable) SetParForChannel(setup.dev.asic,channel,"monitor_LG_disable",0);
		vrange.push_back(3.6*33/Cdet);
	}else{
		printf("Error: wrong scale: %u\n",scale);
		std::list<float> a;
		return a;
	}
	
	//remove out-of range points
	vrange.remove_if(badpoint);
	return vrange;
}
