#include <unistd.h>
#include "Sequencer.h"                                                                               
//#include "MeasurementCollection.h"
#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "config_helpers.h"
#include "HistogramDAQ_client.h"
#include "EventListDAQ_client.h"

//device library
//#include "VPowerSupply.h"
//#include "VPulseGen.h"

#include <iostream>
	using namespace std;
struct{
	struct{
		TIBConfig* config_ib;
		std::vector<TKLauS4Config*> config_asic;
		std::vector<HistogramDAQ*>  daq_histos;
		EventListDAQ*  daq_hits;
		//VPulseGen *pulsegen;
		//VPowerSupply* HV1_supply;
		//VPowerSupply* LED_supply;

		void SetN(int n){
			config_asic.resize(n);
			daq_histos.resize(n);
		}
	} dev;
	std::vector<TTree*> trees;
} setup;


