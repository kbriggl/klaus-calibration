// File: CalibrationBox.h
// Desc: Time calibration of mutrig based data:
// 	 Time aligmnent (coarse, full), DNL correction
// Author: K. Briggl
// 9/20:  Adapted for KLauS6 : E. Warttmann
// 10/20: Added KLauS ADC correction functions: KB

#include "TObject.h"
#include <vector>
#include <map>
#include <array>
#include <inttypes.h>
#include <random>
class klaus_event;
class klaus_acquisition;

//ROOT headers for visualization graphs
#include "TGraph.h"
#include "TH1.h"
#include "TH2.h"

#ifndef _CALIBRATIONBOX_H__
#define _CALIBRATIONBOX_H__
class CalibrationBox
: public TObject
{
public:
	//Build empty calibration box constructor
	CalibrationBox();
	//Build new calibration box and reserve space for N channels with a reference frequency of f in Hz.
	CalibrationBox(int nChannels,double f_ref=160e6,int nBins=128);
	~CalibrationBox();
	int GetNChannels(){return m_finetime_offsets.size();};

	//------------------------ TDC -------------------------

	//Timebase
	void SetFref(double f_ref);
	void SetTtick(double period);
	double GetFref(){return 1./m_tick_period;};
	double GetTtick();

	//Update Time DNL maps including new hit or hits.
	//Important: Recomputation to meaningful histograms is only done after a call to Recompute();
	void AddHit(klaus_event& thehit);
	void AddHits(klaus_acquisition& event);
	void SetHitCounts(CalibrationBox& box);
	int FixBrokenBins();
	int RecomputeTimeDNL();
        bool DNL_Valid() const{return m_DNLcomputed;};

	//Generate ROOT histograms/Graphs for visualization of time CDT data (Note: not stored internally but generated every time) 
	TH1F   GetTimeCountsHist();
	TH1F   GetTimeBinWidthHist(uint16_t channel);
	TH2F   GetTimeBinWidthMap();
	TGraph GetTimeShiftGraph();

	//TODO: currently only time, no ADC
	//Merge data from other into this, appending information at the end.
	void Append(CalibrationBox& other);
	//Merge data from other into this, combining the count values and averaging the offsets. DNL maps are recomputed
	void Merge(CalibrationBox& other);
	//Reset time count histograms - and only these: Not propagated to the results containers, so the class uses the persistent results until the next call of RecomputeDNL().
	void Reset();

	// Set&Get fine time shift values in units of fine ticks
	// This is only applied in the GetTimeXXX() methods, and only if the correstponding bit is set when calling the function
	void SetTimeShift(CalibrationBox& other);
	void SetTimeShift(int channel, float value);
	double GetTimeShift(int channel);
	
	//Compute full time (using hit->GetTime()) based on DNL corrected value, in seconds (or whatever the unit/exponent of the tick_period is)
	//Include fine time shift if bit is set
	//Throws std::out_of_range if no calibration data exists
	double GetTimeDNLCorrected_real(const klaus_event& thehit, bool timeshift_correction) const;

	//Compute dithered and binned time (using hit->GetTime()) based on DNL corrected value,
	//in units of remapped fine counter bins (=1./(32*4) CC bins, 12.5ps for a 625MHz reference clock)
	//Include fine time shift if bit is set
	//Throws std::out_of_range if no calibration data exists
	uint32_t GetTimeDNLCorrected_BDITH(const klaus_event& thehit, bool timeshift_correction);

	double GetBinSize() const; //Get size of TDC bin
	double GetBinSize_BDITH() const; //Get bin size of fine bins returned by the BDITH method.
	int    GetFineBinScaling_BDITH() const; //get ratio of TDC bin size over bin size used in the BDITH method.

	//------------------------ ADC -------------------------
	//ADC calibration database read in and settin and setting
	void ReadADCLinearity_db(std::string);
	void ReadADCLinearity_db_12b(std::string);
	void ReadADCScaling_db(std::string);
	void SetPedestal(int channel, float pedestal, bool gainsel_bit=true); //gainsel_bit==true === LG
	float GetPedestal(int channel, bool gainsel_bit=true); //gainsel_bit==true === LG
	void SetAGScaling(int channel, float scale);
	float GetAGScaling(int channel);

	//Compute dithered and binned charge information (using hit->ADC_10b) based on DNL corrected value,
	//in units of remapped ADC bins (xBDITH_scaling).
	//Throws std::out_of_range if no calibration data exists
	//If bit is set, use stored pedestal and scale values for autogain calibration, remove pedestal.
	//for autogain_scaling, and pedestal removal enabled, the output (ignoring DNL correction) is calculated as
	//      { ADC[-Ped_HG]                                  | gs == 0
	//Q =   {
	//      { (ADC-Ped_LG)*Scaling + Ped_HG [-Ped_HG]       | gs == 1
	int32_t GetChargeDNLCorrected_BDITH(const klaus_event& thehit, int BDITH_scaling=1, bool autogain_scaling=false, bool pedestal_substraction=false);
	int32_t GetChargeDNLCorrected_BDITH_12b(const klaus_event& thehit, int BDITH_scaling=1, bool autogain_scaling=false, bool pedestal_substraction=false);


protected:
	//TDC nonlinearity database
	bool m_DNLcomputed;
	int m_nBins;
	std::vector<float> m_finetime_offsets; //units: TDC bins (fine)
	std::vector<uint64_t> m_code_counts_sum;
	std::vector<std::vector<uint32_t> > m_code_counts;//histogram for counts
	std::vector<std::vector<float> > m_code_times; //! absolute (summed from t_0=0) times for each bin for use in GetTimeDNLCorrected_real & GetTimeDNLCorrected_BDITH
	int m_nChannels;
	int m_nBits;
	double m_tick_period;
	//ADC nonlinearity database
	std::vector<std::vector<float> > m_code_ADCtransitions; //absolute ADC edges for each bin for use in GetChargeDNLCorrected_BDITH
	std::vector<std::map<int,float> > m_code_ADCtransitions_12b; //absolute ADC edges for each bin for use in GetChargeDNLCorrected_BDITH_12b
	std::vector<std::array<float,2>> m_ADC_pedestals; //pedestal database per channel and branch. Agnostic of scaling (HG/MG; LG/ULG) configuration
	std::vector<float> m_ADC_scaling; //relative ADC scaling between HG and LG branch. Agnostic of scaling (HG/MG; LG/ULG) configuration
	//bin dithering random generator
	std::default_random_engine m_randgen; //!
public:
	ClassDef(CalibrationBox,1);
};
#endif
