#ifndef OPTIMIZER_H
#define OPTIMIZER_H
#include <iostream>
#include <vector>
#include <list>
#include "TH1F.h"
#include "TGraph.h"


class   inputbiasOptimizer{
    public:
        // scan informations
        std::list<unsigned short>   channels;
        std::list<unsigned short>   gDACs;
        std::list<unsigned short>   fDACs;

        std::vector<std::vector<TH1F*>> histos;     // raw histogram 
        std::vector<std::vector<TH1F*>> calhistos;  // calibrated histogram 

        unsigned short              gOptDAC;        // global optimized DAC
        std::vector<unsigned short> fOptDACs;       // optimized fDAC
        unsigned short              GMAX;
        unsigned short              FMAX;
        bool                        calibre;

        // constructor
        inputbiasOptimizer(std::list<unsigned short>& chs,
                            std::list<unsigned short>& gdacs,
                            std::list<unsigned short>& fdacs);

        // extract histograms from the raw data files
        //
        void    SetCalibreFlag(bool cal){calibre=cal;};
        void    SetMAX(unsigned short g, unsigned short f){GMAX=g;FMAX=f;};
        // get the histograms from raw data file
        void    ExtractRawHistograms(char* ifilename);      
        // calibrate the histograms
        void    SpectrumCalibration(std::vector<TH1F*> ncdts);           
        // Extract the sps gain
        void    ExtractGain(std::vector<double>& pedVals);  
        // Get the gain graph for one setting
        void    SaveGainAtSetting(unsigned short gDAC, unsigned short fDAC);
        // Save results to file
        void    SaveResults(char* filename);
        // Extract the optimized global DAC
        //void    FindOptimizedGlobalDAC();
        //void    FindOptimizedFineDAC();
        void    FindTargetBiasDAC(double target);
        void    ProcessOptimization(char* filename, std::vector<double>& pedVals, double target);

    private:
        std::vector<unsigned short>         gOpts;      // optimized gDAC values, 36
        std::vector<std::vector<double>>    gains;      // gain matrix, 36*256
        std::vector<std::vector<TGraph*>>   grfits;      // gain matrix, 36*256
        std::vector<double>                 OptGains;   // Optimized 
        TH1F*   freqHist;
        TGraph* grmax;
        TGraph* grraw;
        std::vector<TGraph*>                grchs;
        TGraph*                             grCoef;

        
};

#endif
