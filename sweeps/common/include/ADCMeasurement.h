#include "MeasurementCollection.h"
#include <unistd.h>
 #include "Sequencer.h"                                                                               
#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "config_helpers.h"
#include "EventType.h"
#include "TMath.h"
		  
#include <chrono>
#include <iostream>
using namespace std;

class Measurement: public Seq::MeasurementBase{
	protected:
		TTree* treeHists;
		TTree* treeDump;

		float f_readTime;
		struct{
			klaus_acquisition* curr_aqu=NULL;
		} res;



		void FetchAndFill(){
			if(treeDump==NULL)
				return;
			TList* li=setup.dev.daq_hits->FetchResults();
			if(li==NULL)
				return;

			TObjLink* curr_aqu_entry = li->FirstLink();
			while(curr_aqu_entry){
				res.curr_aqu=(klaus_acquisition*)curr_aqu_entry->GetObject();
				treeDump->Fill();
				curr_aqu_entry = curr_aqu_entry->Next();
			}
		}

	public:

	Measurement(TTree* _treeHists, TTree* _treeDump, float readTime){
		treeHists=_treeHists;
		treeDump=_treeDump;
		if(treeHists!=NULL){
			for(auto daq: setup.dev.daq_histos){
				//printf("Branch connect\n");
				if(daq)
					treeHists->Branch(TString::Format("histos_chip%d",daq->GetBound()),daq->GetResultsPtr());
			}
		}
		if(treeDump!=NULL){
			TBranch* br=treeDump->Branch("acquisitions",&(res.curr_aqu));
		}
		f_readTime=readTime; 	//readTime < 0: read at least readTime events
					//readTime > 0: read at least readTime milliseconds
	};

	~Measurement(){ 
		//if(treeDump!=NULL)
		//	treeDump->GetBranch("acquisitions")->ResetAddress();
		//if(treeHists!=NULL)
		//	for(auto daq: setup.dev.daq_histos)
		//		treeHists->GetBranch(TString::Format("histos_chip%d",daq->GetBound()))->ResetAddress();
	};

	virtual void Prepare(){printf("Measurement::Prepare() - Empty\n");};
	virtual void Cleanup(){printf("Measurement::Cleanup() - Empty\n");};
	void Run(){
		//Prepare graph
		Seq::Settling::Settle();
		setup.dev.daq_hits->FlushFIFO(0);
		setup.dev.daq_histos[0]->ResetResults();
		
		Seq::Settling::Request(10000);
		Seq::Settling::Settle();

		setup.dev.daq_hits->FlushFIFO(0);
		setup.dev.daq_histos[0]->ResetResults();
		setup.dev.daq_hits->ResetResults();

		//negative: read number of events
		if(f_readTime<0){
			int n=-f_readTime;
			printf("ADC Measurement class: Get %1.0f events\n",-f_readTime);
			while(n>0){
				int dn=n;
				if(dn>1000) dn=1000;
				setup.dev.daq_hits->ReadChipUntilEmpty(dn);
				n-=dn;
				//fetch tree data; store
				FetchAndFill();
			}
		//positive: read time in microseconds
		}else{
			auto readTime=f_readTime;
			printf("ADC Measurement class: Reading for %1.2fms\n",f_readTime);
			setup.dev.daq_hits->ReadChipAsyncStart();
			auto start = std::chrono::steady_clock::now();
			auto last = start;
			do{
				FetchAndFill();
				auto dt_last=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - last).count();
				if(dt_last > 5e3){
					printf("Adding %d ms due to inactivity...\n",dt_last);
					readTime+= dt_last;
				}
				last = std::chrono::steady_clock::now();
			}while (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() < readTime);
			setup.dev.daq_hits->ReadChipAsyncStop();
		}
		printf("ADC Measurement class: Measurement finished\n");
		if(treeHists!=NULL){
			printf("ADC Measurement class: Fetching histograms\n");
			for(auto daq: setup.dev.daq_histos)
				daq->FetchResults();
			treeHists->Fill();
		}
		printf("ADC Measurement class: Done\n");
	}
};
