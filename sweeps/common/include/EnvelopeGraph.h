
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"

class EnvelopeGraph{
	public:
		EnvelopeGraph();
		~EnvelopeGraph();
		
		void Reset();
		void Add(const TGraph& gr);
		void Add(const TMultiGraph& gr);

		TGraphErrors* Graph(){return m_graph;};
		
	protected:
		TGraphErrors* m_graph;
};
