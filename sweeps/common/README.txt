


#################### Information on the common subdirectory ###################

This subdirectory contains all necessary classes to measure and analyze all calibrations of the KLauSASIC for a testbeam campaign.
It builds a shared library (.so) which can be included in analasis scripts. 

Check, what is used in the Makefile.configure!!

ADCMeasurement: Used to setup and handle measurements with the ADC, used in ADC linearity measurements. 

AnalyzeADC:  Based on th histogram DAQ, extracts DNL and INL, important for later usage of CalibrationBox.

CalibrationBox: Used for ADC/TDC DNL correction and autogain scaling

CalibreADC: Used in ADC linearity measurements..  

EventType: Defines the event structure of the used ASIC. Definition of the used ASIC (5 - 6 byte difference from K5-K6).

klaus_config_helpers: Necessary for sweeps scans. Possibility of setting parameters.

klaus_constants: Sets channel nr to 36 

Optimizer: Gain Optimizer, not used any more. 

ParamSetters: Setting Parameters for certain optimized steps. Not used any more.

Setup: Needed for setup before scan can start, also used to introduce steered deviced via serial port (pulsegen, HVsupply, ...)

spsGainExtractor: FFT analysis of SPS for gain uniformity calibration.


Important!!! To build the ADCcalib scan, the included libana build has to be changed!!! 
DO: Open Makefile.configure, remove EventType from included objects and dict_headers. Save and run make clear all. Afterwards you can build the ADC scan and change the libana.common Makefile configure
back to its original.!!!

Last Change: E. Warttmann, 18.02.2022
