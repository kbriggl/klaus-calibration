
#include "EnvelopeGraph.h"
#include "TList.h"
EnvelopeGraph::EnvelopeGraph():
	m_graph(NULL)
	{};
EnvelopeGraph::~EnvelopeGraph()
{
	if(m_graph!=NULL) delete m_graph;
};

void EnvelopeGraph::Reset(){
	if(m_graph!=NULL) delete m_graph;
	m_graph=NULL;
}
void EnvelopeGraph::Add(const TGraph& gr){
	//printf("EnvelopeGraph::Add(%p: %s)\n",this,gr.GetName());
	if(m_graph==NULL){
		m_graph=new TGraphErrors(gr.GetN());
		//init graph
		for(int i=0;i<gr.GetN();i++){
			m_graph->SetPoint(i,gr.GetX()[i],gr.GetY()[i]);
			m_graph->SetPointError(i,0,0);
		}
		return;
	}
	if(gr.GetN()!=m_graph->GetN()) throw;

	for(int i=0;i<gr.GetN();i++){
		if(gr.GetX()[i]!=m_graph->GetX()[i]) throw;

		double lo=m_graph->GetY()[i]-m_graph->GetEY()[i];
		double hi=m_graph->GetY()[i]+m_graph->GetEY()[i];

		if(gr.GetY()[i]<lo) lo=gr.GetY()[i];
		if(gr.GetY()[i]>hi) hi=gr.GetY()[i];

		m_graph->GetY()[i]=(hi+lo)/2.;
		m_graph->GetEY()[i]=(hi-lo)/2.;
	}
	return;
}
void EnvelopeGraph::Add(const TMultiGraph& gr){
	TIter next(gr.GetListOfGraphs());
	TGraph* obj;
	while ((obj = (TGraph*)next()))
		this->Add(*obj);
}
