#include <list>
#include <algorithm>
#include <iostream>
#include <vector>

#include "TGraphErrors.h"
#include "TH1.h"
#include "TH1F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TSpectrum.h"
#include "TVirtualFFT.h"
#include "spsGainExtractor.h"
#include "TROOT.h"
using namespace std;

bool    spsGainExtractor::ExtractGainByFFT(int fftmin, int fftmax){
    double gain;
    {auto o=gROOT->FindObject("out_MAG"); if(o) delete o; }
    {auto o=gROOT->FindObject("MAG"); if(o) delete o; }
    TH1*    hm = NULL;
    TVirtualFFT::SetTransform(0);
    hm = sps->FFT(hm, "MAG");              

    TSpectrum* spec = new TSpectrum(1);
    hm->GetXaxis()->SetRange(fftmin,fftmax);
    int nPeaks = 0; nPeaks = spec->Search(hm);
    double spsrange = sps->GetNbinsX();
    if(nPeaks){
        double* xPeaks = spec->GetPositionX();
        gain_fft = spsrange/xPeaks[0];
    }
    else{ return false; } // if extraction failed, return false
    hm->Reset();
    return true;
}

TGraph* spsGainExtractor::ExtractGainByTSpectrum(){
        std::vector<double> pos;
        std::vector<double> poserr; poserr.resize(0);
        TGraphErrors* gr = new TGraphErrors();
        TSpectrum* spec = new TSpectrum(6);
        int     nPeaks = spec->Search(sps,2);
        double* xPeaks = spec->GetPositionX();
        if(nPeaks<=1){
		printf("ExtractGainByTSpectrum() Failed\n");
        	threshold = 0.0;
		return NULL;
	}
        // Estimate the gain
        spsSort(xPeaks,nPeaks);   // sort the peaks
        double estimateGain = (xPeaks[nPeaks-1]-xPeaks[0])/(nPeaks-1);
        int fitrange = (int) estimateGain/2.0-1;
        fitrange = 4;
        if(ExtractGainByFFT()){ estimateGain = gain_fft;   }
        // gauss fit of the 
        for(auto i=0;i<nPeaks;i++){
            TF1* f1 = new TF1("f1","gaus",xPeaks[i]-fitrange,xPeaks[i]+fitrange);
            sps->Fit(f1, "RNQ+");
            pos.push_back(f1->GetParameter(1));
            poserr.push_back(f1->GetParError(1));
        }
        // linear fit of the peaks
        //gr->SetMarkerStyle(8);
        int npe0 = (int)ceil((pos[0]-pedestal)/estimateGain);
        for(int i=0;i<nPeaks;i++){ 
                int npe = npe0 + (int)round((pos[i]-pos[0])/estimateGain);
                gr->SetPoint(gr->GetN(),npe,pos[i]); 
                gr->SetPointError(gr->GetN()-1,0,poserr[i]);
                printf("%d->%d,%.2f +/ %.2f\n",i,npe,pos[i],poserr[i]);
        }
        TF1* f2 = new TF1("f2","pol1");
        gr->Fit(f2,"NQ");
        gain_fit = f2->GetParameter(1);
        gain_err = f2->GetParError(1);
        if(gain_fit-estimateGain>1){
            for(int i=0;i<nPeaks;i++){
                int npe = (int)round((pos[i]-pedestal)/estimateGain);
                printf("ped=%.2f,est=%.2f,fit=%.2f,%d->%d,%.2f\n",pedestal,estimateGain, gain_fit,i,npe,pos[i]);
            }
        }

        // estimated the minimum threshold
        int startBin = (int)pedestal - 5;
        int minContent =(int)(sps->GetBinContent(pos[0]+1)/200) + 5;
        int minBin;
        for(auto i=startBin;i<pos[0];i++){
            if(sps->GetBinContent(i+1)>=minContent){
                minBin = i;
                break;
            }
        }
        if(minBin<(int)pedestal+7) 
            threshold = 0.0;
        else 
            threshold = round(1.0*npe0 - (pos[0]-minBin)/gain_fit)+0.5;
        //printf("find minBin at %d, threshold = %.2f\n", minBin,threshold);

        return gr;
}

void    spsGainExtractor::spsSort(double* a, int n){
    int     index;
    double  temp;
    for(auto i=0;i<n-1;i++){
        index = i;
        for(auto j=i+1;j<n;j++){
            if (a[j]<a[index]) index = j;
        }
        temp = a[i];    a[i] = a[index];    a[index] = temp;
    }
}
