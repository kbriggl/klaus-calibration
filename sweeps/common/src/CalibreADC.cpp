#include <iostream>
#include <map>
#include <list>
#include <vector>
#include <iterator>

#include "CalibreADC.h"

using namespace std;


CalibreADC::CalibreADC(){
        // initial the m_ncdt;
        for(int i=0;i<36;i++) m_ncdt.push_back(new TH1F(Form("CH%d",i),Form("CH%d",i),1024,0,1024));
}

CalibreADC::CalibreADC(const char* ncdtfname){
        TFile* fcdt = new TFile(ncdtfname);
        for(int i=0;i<36;i++){
		TH1F* h=(TH1F*)fcdt->Get(TString::Format("norm/Norm_CH%d",i));
		if(!h){printf("CalibreADC::CalibreADC(%s) - could not find histogram for channel %d\n",i);}
		m_ncdt.push_back(h);
	}
}

void    CalibreADC::SetNormCDT(TH1F* cdt, int channel){
        //m_ncdt[channel]->Add(cdt);
        m_ncdt[channel] = cdt;
}

TH1F*   CalibreADC::GetVariableBinsizedHistogram(TH1F* histo, int channel){
	double	xBinEdge[1025];
	xBinEdge[0] = histo->GetXaxis()->GetBinLowEdge(1);
	for(int i=1;i<1025;i++){
		xBinEdge[i] = xBinEdge[i-1] + m_ncdt[channel]->GetBinContent(i);
	}
	TH1F* htemp = new TH1F("htemp","htemp",1024,xBinEdge);
	for(int i=1;i<1025;i++){ htemp->SetBinContent(i,histo->GetBinContent(i)); }
	return htemp;
}


TH1F* CalibreADC::GetCalibredHistogram(TH1F* histo, int channel){
	double	xBinEdge[1025];
	xBinEdge[0] = histo->GetXaxis()->GetBinLowEdge(1);
	for(int i=1;i<1025;i++){
		xBinEdge[i] = xBinEdge[i-1] + m_ncdt[channel]->GetBinContent(i);
	}
	int start = 1;
	double portion;
	double arragedCount[1024];
	for(int i=0;i<1024;i++) arragedCount[i] = 0.0;
	for(int i=0;i<1024;i++){
		bool calculated = false;
		for(int j=start;j<=1024;j++){
			if(xBinEdge[j]<=i+1){
				portion = 1.0*(xBinEdge[j]-i)/(xBinEdge[j]-xBinEdge[j-1]);
				portion = (portion>1.0)?1.0:portion;
				arragedCount[i] = arragedCount[i] + portion*histo->GetBinContent(j);
				calculated = true;
			}
			else{
				if(calculated==false) arragedCount[i] = 1.0/(xBinEdge[j]-xBinEdge[j-1])*histo->GetBinContent(j);
				else{
					portion = 1.0*(i+1-xBinEdge[j-1])/(xBinEdge[j]-xBinEdge[j-1]);
					portion = (portion>1.0)?1.0:portion;
					arragedCount[i] = arragedCount[i] + portion*histo->GetBinContent(j);
				}
				start = j;
				break;
			}
		}
	}
	TH1F* hcal = new TH1F("hcal","",1024,0,1024);
	for(int i=0;i<1024;i++) hcal->SetBinContent(i+1,arragedCount[i]);
	return hcal;
}

