#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TAxis.h"
#include "TMath.h"
#include <fftw3.h>
#include "AnalyzeADC.h"
using namespace std;


/*
 ******************** Linearity analysis class ***************************
*/
void 	ADCLinearity::AnalysisNonLinearity(std::vector<int>& CDT, int xmin, int xmax){
	int NCode = pow(2,m_Nbit);
	if(CDT.size()!=NCode){
		printf("Error: ADCLinearity::AnalysisNonLinearity(): CDT.size()!=pow(2,m_Nbit)\n"); 
		return;
	}
	// calculate the mean value of the CDT
	double mean = 0.0;
	for(int i=xmin;i<=xmax;i++) mean += CDT[i];
	mean = 1.0*mean/(xmax-xmin+1);

	// calculate the DNL
	m_DNL.resize(0);
	for(int i=0;i<xmin;i++) m_DNL.push_back(0.0);
	for(int i=xmin;i<=xmax;i++) m_DNL.push_back(CDT[i]/mean-1.0);
	for(int i=xmax+1;i<NCode;i++) m_DNL.push_back(0.0);

	// calculate the INL
	m_INL.resize(0);
	m_INL.push_back(0.0);
	for(int i=1;i<NCode;i++) m_INL.push_back(m_INL[i-1]+m_DNL[i-1]);

	// calculate the min/max of the DNL/INL
	m_minDNL = *min_element(m_DNL.begin(),m_DNL.end());
	m_maxDNL = *max_element(m_DNL.begin(),m_DNL.end());
	m_minINL = *min_element(m_INL.begin(),m_INL.end());
	m_maxINL = *max_element(m_INL.begin(),m_INL.end());
}

std::vector<double> 	ADCLinearity::GetDNL(double& minDNL, double& maxDNL){
	minDNL = m_minDNL;
	maxDNL = m_maxDNL;
	return m_DNL;
}

std::vector<double> 	ADCLinearity::GetINL(double& minINL, double& maxINL){
	minINL = m_minINL;
	maxINL = m_maxINL;
	return m_INL;
}

void	ADCLinearity::AnalysisHistoNonLinearity(TH1F* histo, int xmin, int xmax){
	std::vector<int> CDT(pow(2, m_Nbit),0);
	// copy the histogram cdt data into the vector
	//for(int i=0; i<histo->GetMaximumBin();i++){
	for(int i=xmin; i<=xmax;i++){
		//int code = histo->GetBin(i+1);
		//CDT[code] = histo->GetBinContent(i+1);
		CDT[i] = histo->GetBinContent(i+1);
	}
	// Analysis the non-linearity
	this->AnalysisNonLinearity(CDT,xmin,xmax);
}

void	ADCLinearity::GetDNL(TH1F* histo, double& minDNL, double& maxDNL){
	for(int i=0; i<m_DNL.size();i++){ histo->SetBinContent(i+1,m_DNL[i]); }
	minDNL = m_minDNL; maxDNL = m_maxDNL;
}


void	ADCLinearity::GetINL(TH1F* histo, double& minINL, double& maxINL){
	for(int i=0; i<m_INL.size();i++){ histo->SetBinContent(i+1,m_INL[i]); }
	minINL = m_minINL; maxINL = m_maxINL;
}

void	ADCLinearity::GetNorm(TH1F* histo){
	for(int i=0; i<m_DNL.size();i++){ histo->SetBinContent(i+1,m_DNL[i]+1); }
}

/*
 ************************ Dynamic analysis*********************************
 */
ADCDynamic::ADCDynamic(double fs){
        m_fs = fs;
        m_SNR = -1.0; m_SNDR = -1.0; m_ENOB = -1.0; m_SFDR = -1.0;

        // set the default fft analysis parameters
        SetFFTParameters();
}

// set the data array
void    ADCDynamic::SetDataArrayForFFT(vector<int>& ADC, int NSample){
        if(ADC.size()<NSample){
                printf("data size is SMALLER than the required sample number\n");
                return;
        }
        SetNSample(NSample);
        m_ADC = ADC;
}

void    ADCDynamic::SetNSample(int NSample){
        m_NSample = NSample;
        // renew the hanning window
        GetHanningWindow();
        GetBlackmanWindow();
	GetKaiserWindow();
}

void    ADCDynamic::SetFFTParameters(int dcWindow, int signalWindow, int harmonicWindow, int harmonicToneNum){
        m_dcWindow = dcWindow;
        m_signalWindow = signalWindow;
        m_harmonicWindow = harmonicWindow;
        m_harmonicToneNum = harmonicToneNum;
}

// FFT analysis using the fftw library
// --------------------------------------
// windowType:
//              0: -->  no window applied
//              1: -->  Hanning window
//              2: -->  4-term Blackman-Harris window
//              3: -->  Kaiser window
//-------------------------------------
void    ADCDynamic::FFT1D(int windowType, double kaiserAlpha){
	// get rid of the dc power in the data
	vector<double> ADC;
	double mean = 0.0;
	for(int i=0;i<m_ADC.size();i++) mean += m_ADC[i];
	mean = mean/m_ADC.size();
	for(int i=0;i<m_ADC.size();i++) ADC.push_back(m_ADC[i]-mean);

        fftw_complex *x; // input array
        fftw_complex *y; // output array
        x = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*m_NSample);
        y = (fftw_complex*)fftw_malloc(sizeof(fftw_complex)*m_NSample);

        for(int i=0;i<m_NSample;i++){
                if(windowType==1) x[i][0] = ADC[i]*m_Hanning[i];     	// hanning windows
                else if(windowType==2) x[i][0] = ADC[i]*m_Blackman[i]; 	// 4-term Blackman-Harris window
                else if(windowType==3){
			GetKaiserWindow(kaiserAlpha);
		       	x[i][0] = ADC[i]*m_Kaiser[i];   	// Kaiser window for REAL parts
		}
                else x[i][0] = ADC[i];     // REAL parts

                x[i][1] = 0.0;          // IMAG parts
        }
        fftw_plan plan = fftw_plan_dft_1d(m_NSample, x, y, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);
        // get the data out to the m_fft
        m_fft.resize(0);
        for(int i=0;i<m_NSample;i++){
                //printf("FFT result: FFT[%d]=%.4f\n",i,sqrt(y[i][0]*y[i][0]+y[i][1]*y[i][1]));
                //m_fft.push_back(sqrt(y[i][0]*y[i][0]+y[i][1]*y[i][1]));
                m_fft.push_back(y[i][0]*y[i][0]+y[i][1]*y[i][1]);
        }
        //fftw_cleanup();
        fftw_free(x); fftw_free(y);
}

// Analysis the FFT results from the m_fft
// Not that the m_fft is aleady the power, so no need to square 2
void    ADCDynamic::AnalysisFFTResults(){
	printf("\n\n****************************************\n");
	printf("\t\t\t Analysis the FFT results\n");
	printf("\n\n****************************************\n");
        // find the maximum
        int maxIndex;
        double maxFFTValue = -1.0;
        for(int i=1;i<m_NSample/2;i++){ // ignore the DC
                if(m_fft[i]>maxFFTValue){ maxIndex = i; maxFFTValue = m_fft[i]; }
        }
        printf("Find maximun at [%d] = %.4f\n",maxIndex, maxFFTValue);

        // normalize the power spectra
        vector<double> fftNormalized; // normalized fft value;
        for(int i=0;i<m_NSample/2;i++) fftNormalized.push_back(m_fft[i]/maxFFTValue);

        // calculate the dBFS vector
        m_fftdBFS.resize(0);
        for(auto i=0;i<fftNormalized.size();i++){
                m_fftdBFS.push_back(10*log10(fftNormalized[i]));
        }

        // calculate the SFDR
        double max2FFTValue = -1;
	int SFDRindex = 0;
        for(int i=m_dcWindow;i<m_NSample/2;i++){
                if(i>=maxIndex - m_signalWindow && i<=maxIndex + m_signalWindow) continue;
                if(fftNormalized[i]>max2FFTValue) {
			max2FFTValue = fftNormalized[i];
			SFDRindex = i;
		}
        }
        if(max2FFTValue>0){
		printf("\n\t\t Find the SFDR at %d\n",SFDRindex);
	       	m_SFDR = 10*log10(1.0/max2FFTValue);
	}
        
        // calculate the total power
        double Ptot = 0.0;
        for(unsigned int i=0;i<fftNormalized.size();i++) Ptot += fftNormalized[i];
	printf("\t\t Total power=%.6f\n",Ptot);

        // calculate the DC power
        double Pdc = 0.0; for(int i=0;i<m_dcWindow;i++) Pdc += fftNormalized[i];
	printf("\t\t DC power=%.6f\n",Pdc);

        // calclulate the signal power
        double Psignal = 0.0;
        for(int i=maxIndex-m_signalWindow;i<=maxIndex+m_signalWindow;i++){
                if(i<0) continue;
                Psignal += fftNormalized[i];
		printf("\t\tsignal spectrum: %d, %.6f\n",i,fftNormalized[i]);
        }
	printf("\t Signal power=%.6f\n",Psignal);


        // calculate the harmonic power
        vector<double> hdIndex;
        vector<double> hdPower;
        for(int i=0;i<m_harmonicToneNum;i++){
                int tone = (i+1)*maxIndex; //
                if(tone>=m_NSample) break;
                else if(tone>=m_NSample/2) tone = m_NSample - tone;
                hdIndex.push_back(tone);

                // get the tone power
                double  hdtonepower = 0.0;
                for(int j=tone-m_harmonicWindow;j<=tone+m_harmonicWindow;j++){
                        if(j>=0 && j<=m_NSample/2) hdtonepower += fftNormalized[j];
                }
                hdPower.push_back(hdtonepower);
                printf("Harmonic %d, tone at %d, power = %.6f\n",i,tone,hdtonepower);
        }
        double Phd = 0.0;
        for(unsigned int i=1;i<hdPower.size();i++) Phd += hdPower[i];// the first one is the signal

        double Pnoise = Ptot - Pdc - Psignal - Phd;

        printf("\n\tPtot = %.5f, Pdc=%.5f, Psignal=%.5f, Phd=%5.f, Pnoise=%.6f\n",Ptot, Pdc, Psignal, Phd, Pnoise);
        
        // result;
        m_SNR = 10*log10(Psignal/Pnoise);
        m_SNDR = 10*log10(Psignal/(Pnoise+Phd));
        m_ENOB = (m_SNDR - 1.76)/6.02;
}

void    ADCDynamic::DynamicAnalysis(int flag, double kaiserAlpha){
	printf("->Start FFT analysis\n");
        FFT1D(flag,kaiserAlpha);
        AnalysisFFTResults();
        PrintFFTResults();
}

// FFT related results
vector<double>  ADCDynamic::GetFFT(){ return m_fftdBFS; }

void    ADCDynamic::PrintFFTResults(){
//      printf("\n\n\t--------------------------------------------------------\n");
//      printf(    "\t----------------- ADC dynamic results ------------------\n");
//      printf(    "\t--------------------------------------------------------\n");
        if(m_ENOB>0) printf("ENOB=%.2f\t",m_ENOB); else printf("ENOB=xx\t");
        if(m_SNR >0) printf(" SNR=%.2f\t",m_SNR ); else printf(" SNR=xx\t");
        if(m_SNDR>0) printf("SNDR=%.2f\t",m_SNDR); else printf("SNDR=xx\t");
        if(m_SFDR>0) printf("SFDR=%.2f\t",m_SFDR); else printf("SFDR=xx\t");
        printf("\n");
}


// window related function
// Hanning window
// see https://de.mathworks.com/helps/signal/hann.html
// flag = 0: symmeteric --> use this option when using windows for filter design
// flag = 1: periodic   --> use this option for spectra analysis
void    ADCDynamic::GetHanningWindow(int flag){
        m_Hanning.resize(0); // reset the hanning window
        for(int i=0;i<m_NSample;i++) m_Hanning.push_back(0.0);
        
        int half, idx, n;

        if(flag==1) n = m_NSample - 1; // periodic
        else n = m_NSample;

        if(n%2==0){
                half = n/2;
                for(int i=0;i<half;i++) m_Hanning[i] = 0.5*(1 - cos(2*M_PI*(i+1)/(n+1)));
                idx = half -1;
                for(int i=half;i<n;i++) {m_Hanning[i]=m_Hanning[idx]; idx--;}
        }
        else{
                half = (n+1)/2;
                for(int i=0;i<half;i++) m_Hanning[i] = 0.5*(1 - cos(2*M_PI*(i+1)/(n+1)));
                idx = half -2;
                for(int i=half;i<n;i++) {m_Hanning[i]=m_Hanning[idx]; idx--;}
        }

        if(flag==1){ // periodic
                for(int i=m_NSample-1;i>=1;i--) m_Hanning[i] = m_Hanning[i-1];
                m_Hanning[0] = 0.0;
        }
}

void    ADCDynamic::GetBlackmanWindow(){
        m_Blackman.resize(0);
        for(int i=0;i<m_NSample;i++) m_Blackman.push_back(0.0);
        double a0 = 0.35875;
        double a1 = 0.48829;
        double a2 = 0.14128;
        double a3 = 0.01168;
        double val;
        for(int i=0;i<m_NSample;i++){
                //val = a0 - a1*cos(2*M_PI*i/(m_NSample-1)) + a2*cos(4*M_PI*i/(m_NSample-1)) - a3*cos(6*M_PI*i/(m_NSample-1));
                val = a0 - a1*cos(2*M_PI*i/(m_NSample)) + a2*cos(4*M_PI*i/(m_NSample)) - a3*cos(6*M_PI*i/(m_NSample));
                m_Blackman[i] = val;
        }
}

// Kaiser window
void 	ADCDynamic::GetKaiserWindow(double alpha){
	m_Kaiser.resize(0);
	for(int i=0;i<m_NSample;i++) m_Kaiser.push_back(0.0);
	double val;
	for(int i=0;i<m_NSample;i++){
		val = TMath::BesselI0(M_PI*alpha*sqrt(1-(2*i/(m_NSample-1)-1)*(2*i/(m_NSample-1)-1)));
		val = val/TMath::BesselI0(M_PI*alpha);
		m_Kaiser[i] = val;
	}
}




/*
 *  ++++++++++++++++++++++++ Implemented of PipeBranchMerger Class ++++++++++++++++++++++++++
*/

PipeBranchMerger::PipeBranchMerger(){
        // set the default offset distance to be 128 between branches
	for(int i=0;i<64;i++){ m_Offset.push_back(128*(i/2)); }
}


// generate the 14b data array from m_ADC6b and m_ADC8b
void 	PipeBranchMerger::Set14bDataArray(std::vector<int>& ADC6b, std::vector<int>& ADC8b){
        m_ADC6b = ADC6b;
        m_ADC8b = ADC8b;
	if(m_ADC6b.size()!=m_ADC8b.size()){
		printf("PipeBranchMerger::Set14bDataArray(): m_ADC6b.size!=m_ADC8b.size()\n");
		return;
	}
	int code;
	m_ADC14bCDT.resize(8192,0);

	// get the cdt of 14b array
	int Num = m_ADC6b.size();
	for(int i=0;i<Num;i++){
		code = 256*(int(m_ADC6b[i]/2)) + m_ADC8b[i];
		m_ADC14bCDT[code] = m_ADC14bCDT[code] + 1;
	}
}

// Find the edge of a 8b vector
bool PipeBranchMerger::FindEdgeOf8bCDTArray(std::vector<int> &histo, int& rEdge, int& fEdge){
	rEdge = -1; fEdge = -1;
	double meanValue = std::accumulate(std::begin(histo),std::end(histo),0)/histo.size();
	for(int i=0;i<160;i++){ if(histo[i]>meanValue){rEdge = i;break;} }
	for(int i=255;i>100;i--){ if(histo[i]>meanValue) {fEdge = i;break;} }
	
	//printf("\t\tRiseEdge=%d\tFallEdge=%d\n",rEdge,fEdge);
	if(rEdge==-1|fEdge==-1) return false;
	return true;
}

// Find the offset between two neighboor branches by minimized the RMS value of the merged results
// return value:
// 	-1: no meaningful offset found
// 	otherwise: 	optimum offset
int PipeBranchMerger::FindOffsetForNeighboorBranches(std::vector<int> &h1, std::vector<int> &h2){
	// find the edge of each histogram
	int rEdge1, fEdge1, rEdge2, fEdge2;
	FindEdgeOf8bCDTArray(h1,rEdge1,fEdge1);
	FindEdgeOf8bCDTArray(h2,rEdge2,fEdge2);
	if(rEdge1*rEdge2*fEdge1*fEdge2<0) return -1;

	//int initialOfset = 256 + rEdge2 - fEdge1;
	int initialOfset = -rEdge2 + fEdge1;
	int offsetRanges = 20; 	//scan the offset from [initialOfset-offsetRanges, initialOfset+offsetRanges];
	int scanRange = 50; 	//for h1, data from [fEdge1-scanRange, fEdge1+scanRange] is used
	
	vector<double> rmsValues;
	std::vector<int> histo;
	for(int offset=initialOfset-offsetRanges; offset<=initialOfset+offsetRanges; offset++){
		// slice the h1 data from [fEdge1-scanRange,fEdge1+scanRange] into a histogram
		histo.resize(0);
		for(int code=fEdge1-scanRange; code<=fEdge1+scanRange;code++){
			int histoCode = code - (fEdge1-scanRange) + 1;
			int histoValue = h1[code];
			//if(code+offset-256>=0) histoValue += h2[code+offset-256];
			if(code+offset-256>=0) histoValue += h2[code-offset];
			histo.push_back(histoValue);
		}
		// calculate the criterion
		double meanValue = std::accumulate(std::begin(histo),std::end(histo),0)/histo.size();
		double rmsValue = 0.0;
		std::for_each(std::begin(histo),std::end(histo),[&](const int d){rmsValue+=(d-meanValue)*(d-meanValue);});
		rmsValues.push_back(rmsValue);
		//printf("offset:%d\tmeanValue:%f\trmsValue:%f\n",offset,meanValue,rmsValue);
	}
	// find the offset with mimimum criterion
	std::vector<double>::iterator minResult = std::min_element(std::begin(rmsValues),std::end(rmsValues));
	int optimumOffset = initialOfset - offsetRanges + std::distance(std::begin(rmsValues),minResult);
	//printf("\t\t optimumOffset=%d\n",optimumOffset);
	return optimumOffset;
}


// MOST important part for the pipedata reconstruction
// Basic process:
// 	1. 	Slice the 14b CDT array into 8b vectors
// 	2. 	Find the offset for neighboor branches
void 	PipeBranchMerger::GetMerged12bDataArray(){
	int countThreshold = 128*100; // for each 8b bin in one branch, at least 100 entries

	std::vector<int> h1(256,0);
	std::vector<int> h2(256,0);

	std::vector<int> diffOffset(32,128);  // 
	diffOffset[0] = 0; 	// no offset for the first branch

	int startBranch = -1;
	int endBranch=-1;
	int startVaule = 0, endValue=0;
	// find the offset between branches; if not found, keep the default 128
	for(int br=0; br<31; br++){
		// slice 
		h1.resize(256,0);
		for(int code=0; code<256; code++){ h1[code] = m_ADC14bCDT[code+256*br]; }
		int h1Sum = std::accumulate(std::begin(h1),std::end(h1),0);

		// not enough data in the branch, so skip this and start from next branch
		if(h1Sum<countThreshold) continue;

		h2.resize(256,0);
		for(int code=0; code<256; code++){ h2[code] = m_ADC14bCDT[code+256*br+256]; }
		int h2Sum = std::accumulate(std::begin(h2),std::end(h2),0);

		// start branch
		if(3*h1Sum>h2Sum){
			if(startBranch<0){
				startBranch = br;
				int rEdge, fEdge;
				if(FindEdgeOf8bCDTArray(h1,rEdge,fEdge)) startVaule = rEdge;
				printf("Find the Start Branch is %d with Start 8b Code is %d\n", startBranch, startVaule);
			}
		}

		// last branch
		if(br==30){ // already the true last branch
			if(h2Sum>h1Sum/3){ // and have enough data in the branch31
				endBranch = br+1;
				int rEdge, fEdge;
				if(FindEdgeOf8bCDTArray(h2,rEdge,fEdge)) endValue = fEdge; // it is
				printf("Find the End Branch is %d with End 8b Code is %d\n", endBranch, endValue);
			}
		}
		else{ 	// not the true last branch
			if(h2Sum<h1Sum/3){ // but there is no enough data in this branch
				endBranch = br;
				int rEdge, fEdge;
				if(FindEdgeOf8bCDTArray(h1,rEdge,fEdge)) endValue = fEdge; // it is
				printf("Find the End Branch is %d with End 8b Code is %d\n", endBranch, endValue);
			}
			break; // end the loop
		}

		// find the offset between two branches
		int offset = -1;
		offset = FindOffsetForNeighboorBranches(h1,h2);
		printf("\t\t\t  Find offset for %d  = %d\n",br,offset-1);
		diffOffset[br+1] = offset;
	}

	// calculate the Offset
	m_Offset[0] = 0; m_Offset[1] = 0;
	for(int br=1;br<32;br++){
		m_Offset[2*br] = m_Offset[2*br-1] + diffOffset[br];
		m_Offset[2*br+1] = m_Offset[2*br];
	}

	// Get the Merged 12b CDT
	int mergedcode;
	m_ADC12bCDT.resize(8192,0);
	for(int i=0;i<m_ADC6b.size();i++){
		int br = m_ADC6b[i];
		int va = m_ADC8b[i];
		if(br<startVaule || br>endBranch) continue;
		mergedcode = m_Offset[br] + va;
		m_ADC12bCDT[mergedcode] = m_ADC12bCDT[mergedcode] + 1;
	}

	m_12bStart = startVaule + m_Offset[startBranch];
	m_12bEnd   = endValue   + m_Offset[endBranch];
	printf("\t\t Mearged 12b code Start=%d, End=%d\n",m_12bStart, m_12bEnd);
}
