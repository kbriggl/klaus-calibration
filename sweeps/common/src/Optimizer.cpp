#include <list>
#include <map>
#include <algorithm>
#include <iostream>
#include <vector>

#include "TTree.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TEfficiency.h"

#include "HistogrammedResults.h"
#include "EventType.h"
#include "AnalyzeADC.h"
#include "CalibreADC.h"
#include "spsGainExtractor.h"
using namespace std;

class   inputbiasOptimizer{
    public:
        // scan informations
        std::list<unsigned short>   channels;
        std::list<unsigned short>   gDACs;
        std::list<unsigned short>   fDACs;

        std::vector<std::vector<TH1F*>> histos;     // raw histogram 
        std::vector<std::vector<TH1F*>> calhistos;  // calibrated histogram 

        unsigned short              gOptDAC;        // global optimized DAC
        std::vector<unsigned short> fOptDACs;       // optimized fDAC
        unsigned short              GMAX;
        unsigned short              FMAX;
        bool                        calibre;

        // constructor
        inputbiasOptimizer(std::list<unsigned short>& chs,
                            std::list<unsigned short>& gdacs,
                            std::list<unsigned short>& fdacs);

        // extract histograms from the raw data files
        //
        void    SetCalibreFlag(bool cal){calibre=cal;};
        void    SetMAX(unsigned short g, unsigned short f){GMAX=g;FMAX=f;};
        // get the histograms from raw data file
        void    ExtractRawHistograms(char* ifilename);      
        // calibrate the histograms
        void    SpectrumCalibration(std::vector<TH1F*> ncdts);           
        // Extract the sps gain
        void    ExtractGain(std::vector<double>& pedVals);  
        // Get the gain graph for one setting
        void    SaveGainAtSetting(unsigned short gDAC, unsigned short fDAC);
        // Save results to file
        void    SaveResults(char* filename);
        // Extract the optimized global DAC
        //void    FindOptimizedGlobalDAC();
        //void    FindOptimizedFineDAC();
        void    FindTargetBiasDAC(double target);
        void    ProcessOptimization(char* filename, std::vector<double>& pedVals, double target);

    private:
        std::vector<unsigned short>         gOpts;      // optimized gDAC values, 36
        std::vector<std::vector<double>>    gains;      // gain matrix, 36*256
        std::vector<std::vector<TGraph*>>   grfits;      // gain matrix, 36*256
        std::vector<double>                 OptGains;   // Optimized 
        TH1F*   freqHist;
        TGraph* grmax;
        TGraph* grraw;
        std::vector<TGraph*>                grchs;
        TGraph*                             grCoef;

        
};

void    inputbiasOptimizer::ProcessOptimization(char* filename, std::vector<double>& pedVals, double target){
    this->ExtractRawHistograms(filename);
    //this->SpectrumCalibration() ??
    this->ExtractGain(pedVals);
    this->FindTargetBiasDAC(target);
    this->SaveGainAtSetting(0,120);
    this->SaveResults(filename);
}



inputbiasOptimizer::inputbiasOptimizer( std::list<unsigned short>& chs,
                                        std::list<unsigned short>& gdacs,
                                        std::list<unsigned short>& fdacs){
            channels    = chs; 
            gDACs       = gdacs; 
            fDACs       = fdacs;
            SetMAX(1,256);
            SetCalibreFlag(false);
}

void    inputbiasOptimizer::ExtractRawHistograms(char* ifilename){
    // prepare the tree
    TFile* fi = new TFile(ifilename);
    TTree* scan = (TTree*)fi->Get("scan");
    HistogrammedResults* res = NULL;
    scan->SetBranchAddress("histos_chip64",&res);
    unsigned short gDAC;    scan->SetBranchAddress("gDAC",&gDAC);
    unsigned short fDAC;    scan->SetBranchAddress("fDAC",&fDAC);

    // read the histogram
    for(auto i=0;i<36;i++){
        vector<TH1F*> temp; temp.resize(0);
        for(auto j =0;j<GMAX;j++){
            for(auto k=0;k<FMAX;k++){
                temp.push_back(new TH1F(    Form("HD_CH%d_gDAC%d_fDAC%d",i,j,k),
                                            Form("HD_CH%d_gDAC%d_fDAC%d",i,j,k), 1024,0,1024));
            }
        }
        histos.push_back(temp);
    }
    printf("start reading the raw data from file%s\n",ifilename);
    for(auto i=0;i<scan->GetEntries();i++){
            scan->GetEntry(i);
            //int index = GMAX*gDAC+fDAC;
            //printf("index=%d, gDAC=%d, fDAC=%d\n",index,gDAC,fDAC);
            for(auto channel : channels)
                histos[channel][GMAX*gDAC+fDAC]->Add(res->h_ADC_10b[channel]);
    }
    printf("Finish readout the raw data file\n");
}

void    inputbiasOptimizer::SpectrumCalibration(std::vector<TH1F*> ncdts){
    // prepare the histograms
    for(auto i=0;i<36;i++){
        vector<TH1F*> temp; temp.resize(0);
        for(auto j =0;j<GMAX;j++){
            for(auto k=0;k<FMAX;k++){
                temp.push_back(new TH1F(    Form("Cal_HD_CH%d_gDAC%d_fDAC%d",i,j,k),
                                            Form("Cal_HD_CH%d_gDAC%d_fDAC%d",i,j,k), 1024,0,1024));
            }
        }
        calhistos.push_back(temp);
    }
    // Setup the CalibreADC
    CalibreADC* calEngine = new CalibreADC();
    for(int i=0;i<36;i++)  calEngine->SetNormCDT(ncdts[i],i);
    // calibration
    for(auto ch : channels){
        for(auto g : gDACs){
            for(auto f : fDACs){
                calhistos[ch][GMAX*g+f] =calEngine->GetCalibredHistogram(histos[ch][GMAX*g+f],ch);
            }
        }
    }
    
}

void    inputbiasOptimizer::ExtractGain(std::vector<double>& pedVals){
    // prepare the vector
    for(auto i=0;i<36;i++) gains.push_back(std::vector<double>(256,0.0));
    for(auto i=0;i<36;i++) grchs.push_back(new TGraph());
    for(auto i=0;i<36;i++){
        vector<TGraph*> temp; temp.resize(0);
        for(auto j =0;j<GMAX;j++){
            for(auto k=0;k<FMAX;k++){
                temp.push_back(new TGraph());
            }
        }
        grfits.push_back(temp);
    }
    spsGainExtractor* spsEngine = new spsGainExtractor();
    // gain Extraction
    for(auto ch : channels){
        grchs[ch]->SetName(Form("GAIN_CH%d",ch));
        for(auto g : gDACs){
            for(auto f : fDACs){
                // setup the spsGainExtractor
                printf("channel%d, gDAC%d,fDAC%d\n",ch,g,f);
                spsEngine->SetPedestalValue(pedVals[ch]);
                if(calibre)     spsEngine->SetHistogram(calhistos[ch][GMAX*g+f]);
                else            spsEngine->SetHistogram(histos[ch][GMAX*g+f]);
                grfits[ch][GMAX*g+f] = spsEngine->ExtractGainByTSpectrum();
                gains[ch][GMAX*g+f] = spsEngine->gain_fit;
                grchs[ch]->SetPoint(grchs[ch]->GetN(),GMAX*g+f,spsEngine->gain_fit);
            }
        }
    }
    printf("ExtractGain() done!\n");
}

void    inputbiasOptimizer::FindTargetBiasDAC(double target){
    grCoef = new TGraph(); grCoef->SetName("coef");
    fOptDACs = std::vector<unsigned short>(36,127);
    for(auto ch=0; ch<36; ch++){
        if(grchs[ch]->GetN()<3) continue;
        TF1* f1 = new TF1("f1","pol1",25,225);
        grchs[ch]->Fit(f1,"NQ");
        grCoef->SetPoint(grCoef->GetN(),ch,f1->GetParameter(1));
        int optBias = (int)(target-f1->GetParameter(0))/(f1->GetParameter(1));
        fOptDACs[ch] = (unsigned short)optBias;
        if(optBias>220){ printf("Oohs! optBias=%d",optBias); fOptDACs[ch]=220; }
        if(optBias<30){ printf("Oohs! optBias=%d",optBias); fOptDACs[ch]=30; }
    }
}

/*
void    inputbiasOptimizer::FindOptimizedGlobalDAC(){
    gOpts.resize(0);
    freqHist = new TH1F("freqHist","freqHist",16,0,16);
    for(auto ch: channels){
        std::vector<double> temp = gains[ch];
        int maxIndex = std::max_element(temp.begin(),temp.end()) - temp.begin();
        int gDAC = (int)(maxIndex/16);
        gOpts.push_back(gDAC);
        freqHist->Fill(gDAC);
    }
    // find the most frequent gDAC value
    gOptDAC = freqHist->GetMaximumBin()-1;
}

void    inputbiasOptimizer::FindOptimizedFineDAC(){
    OptGains.resize(0);
    fOptDACs = std::vector<unsigned short>(36,7);
    grmax = new TGraph(); grmax->SetName("Gain_Opt");
    for(auto ch : channels){
        std::vector<double> temp; temp.resize(0);
        for(int j=0;j<16;j++) temp.push_back(gains[ch][GMAX*gOptDAC+j]);
        int maxIndex = std::max_element(temp.begin(),temp.end()) - temp.begin();
        fOptDACs[ch] = (unsigned short)maxIndex;
        OptGains.push_back(temp[maxIndex]);
        grmax->SetPoint(grmax->GetN(),ch,temp[maxIndex]);
    }
}
*/




void    inputbiasOptimizer::SaveGainAtSetting(unsigned short gDAC, unsigned short fDAC){
    grraw = new TGraph(); grraw->SetName(Form("Gain_HD_gDAC%d_fDAC%d",gDAC,fDAC));
    for(auto ch: channels){
        grraw->SetPoint(grraw->GetN(),ch,gains[ch][GMAX*gDAC+fDAC]);
    }
}

void    inputbiasOptimizer::SaveResults(char* filename){
    char ofname[255]; sprintf(ofname,"ana-%s",filename);
    TFile* ofile = new TFile(ofname,"recreate");   // output filename
    //freqHist->Write();
    //grmax->Write();
    grraw->Write();
    grCoef->Write();
    TDirectory* rawdir = ofile->mkdir("rawdir"); rawdir->cd();
    for(unsigned int i=0;i<36;i++){
        for(auto kk : histos[i])
            if(kk->GetEntries()>500){ 
                kk->GetXaxis()->SetRange(650,1022);
                kk->Write();
            }
    }
//    TDirectory* caldir = ofile->mkdir("caldir"); caldir->cd();
//    for(unsigned int i=0;i<36;i++){
//        for(auto kk : calhistos[i])
//            if(kk->GetEntries()>500) 
//                kk->Write();
//    }
    TDirectory* gaindir = ofile->mkdir("gaindir"); gaindir->cd();
    for(auto kk : grchs) if(kk->GetN()>1) kk->Write();
    // save the fits
    TDirectory* fitdir = ofile->mkdir("fitdir"); fitdir->cd();
    for(unsigned int i=0;i<36;i++){
        for(auto j=0;j<GMAX;j++){
            for(auto k=0;k<FMAX;k++){
                if( grfits[i][GMAX*j+k]->GetN()>1){
                    grfits[i][GMAX*j+k]->SetName(Form("CH%d_gDAC%d_fDAC%d",i,j,k));
                    grfits[i][GMAX*j+k]->Write();
                }
            }
        }
    }

    ofile->Close();
}
