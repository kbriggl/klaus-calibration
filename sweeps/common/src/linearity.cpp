#include <map>
#include <algorithm>
#include "TTree.h"
#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TMath.h"

//generate dnl plot from graph and slope
TGraph GenDNL(TGraph* gr_in, double slope){
		//compute simple inl/dnl from full range
		TGraph g_dnl(*gr_in);
		g_dnl.SetName((TString (gr_in->GetName())).ReplaceAll("gr_","dnl_"));
		for (int p=0;p<g_dnl.GetN()-1;p++){
			g_dnl.GetY()[p]=(gr_in->GetY()[p+1]-gr_in->GetY()[p])/slope - 1;
		}
		g_dnl.RemovePoint(g_dnl.GetN()-1);
		return g_dnl;
}

//generate inl plot from graph and function
TGraphErrors GenINL(TGraphErrors* gr_in, TF1 func){
		//compute simple inl/dnl from full range
		TGraphErrors g_inl(*gr_in);
		g_inl.SetName((TString (gr_in->GetName())).ReplaceAll("gr_","dnl_"));
		for (int p=0;p<g_inl.GetN();p++){
			g_inl.GetY()[p]-=func.Eval(gr_in->GetX()[p]);
		}
		return g_inl;
}

//generate relative inl plot from graph and function
TGraphErrors GenINLrel(TGraphErrors* gr_in, TF1 func){
		//compute simple inl/dnl from full range
		TGraphErrors g_inl(*gr_in);
		double min, max; func.GetRange(min,max);
		g_inl.SetName((TString (gr_in->GetName())).ReplaceAll("gr_","dnl_"));
		for (int p=0;p<g_inl.GetN();p++){
			g_inl.GetY()[p]-=func.Eval(gr_in->GetX()[p]);
			g_inl.GetY()[p]/=func.Eval(max)-func.Eval(min);
		}
		return g_inl;
}



//generate linear fit of data in gr_in where the inl is less than inl_limit[%] FSR
//results stored in res_* pointers (if non-null, will delete first)
TF1 inl_limit_fit(TGraphErrors* gr_in, double inl_limit, bool subrangecheck=false){
		int range_low;
		int range_high;
		int center;
		Double_t inl,inl_max;
		Double_t chi2_lhs;
		Double_t chi2_rhs;

		//setup results
		TGraphErrors* gr_res=new TGraphErrors();
		gr_res->SetName("gtmp");

		//setup functionu
		TF1 flin("flin","pol1",0,1e9);
		flin.SetParNames("ped","slope");
		flin.SetParameter(0,0.6);
		flin.SetParameter(1,.18);

		//copy manually
		for(int n=0;n<gr_in->GetN();n++){
			gr_res->SetPoint(n,gr_in->GetX()[n],gr_in->GetY()[n]);
			gr_res->SetPointError(n,gr_in->GetEX()[n],gr_in->GetEY()[n]);
		}
		double fsr;
		//fit
		range_low=0;
		range_high=gr_res->GetN()-1;
		while (range_high>range_low){
			//set range, fit for current range
			flin.SetRange(gr_res->GetX()[range_low],gr_res->GetX()[range_high]);
			gr_res->Fit(&flin,"QR0");
			
			//get fsr
			fsr=gr_res->GetY()[range_high]-flin.GetParameter("ped");
			//fsr_Q=gr_res->GetX()[range_high]-gr_res->GetX()[range_low];

			//get max INL - check in [low:high] range
			inl_max=0;
			for (int k=range_low;k<=range_high;k++){
				inl=TMath::Abs((gr_res->GetY()[k] - flin.Eval(gr_res->GetX()[k]))/fsr);
				if (inl_max< inl)
					inl_max=inl;
			}
			
			if (inl_max<inl_limit*0.01)
				break;
			
			printf("[%d : %d] FSR=%f-> INL=%f \n",range_low,range_high,fsr,inl_max);
			
			center=(gr_res->GetX()[range_low]+gr_res->GetX()[range_high])/2;
			//lhs chi2
			if(subrangecheck)
				flin.SetRange(gr_res->GetX()[range_low],center);
			else
				flin.SetRange(gr_res->GetX()[range_low+1],gr_res->GetX()[range_high]);
			gr_res->Fit(&flin,"QR0");
			chi2_lhs=gr_res->Chisquare(&flin);


			//rhs chi2
			if(subrangecheck)
				flin.SetRange(center,gr_res->GetX()[range_high]);
			else
				flin.SetRange(gr_res->GetX()[range_low],gr_res->GetX()[range_high-1]);
			gr_res->Fit(&flin,"QR0");
			chi2_rhs=gr_res->Chisquare(&flin);
			if(subrangecheck){	
				//if (chi2_lhs*8<chi2_rhs){
				if (chi2_lhs*8>chi2_rhs){
					range_low++;
				}else{
					range_high--;		
				}
			}else{
				//if (chi2_lhs<chi2_rhs){
				if (chi2_lhs>chi2_rhs){
					range_low++;
				}else{
					range_high--;		
				}
			}
		}
		//found range. Add final fit function, produce inl (gr_res_inl)
		
		printf("[%d : %d] --> FSR=%f\t\t@INL=%f%%\n",range_low,range_high,fsr,inl_max);

		/*	
		TF1 flin_full(flin);
		flin_full.SetRange(0,100000000);
		flin_full.SetLineColor(kGreen);
		gr_res->GetListOfFunctions()->Add(flin_full.Clone());
		gr_res->GetListOfFunctions()->Add(flin.Clone());

		//calculate residual graph
		for (int n=0;n<gr_res->GetN();n++){
			gr_res_inl->SetPoint(n,gr_in->GetX()[n],gr_in->GetY()[n]-flin.Eval(gr_res->GetX()[n]));
			gr_res_inl->SetPointError(n,gr_in->GetEX()[n],gr_in->GetEY()[n]);
		}
		TF1 flin_inl(flin);
		flin_inl.SetParameters(0,0);
		gr_res_inl->GetListOfFunctions()->Add(flin_inl.Clone());
		slope=flin.GetParameter(1);
		slope_err=flin.GetParError(1);
*/
		//store results
		gr_in->Fit(&flin,"R+");
		delete gr_res;
		return flin;

}

TGraphErrors BitDeltaRel(TGraphErrors& g_bitDelta){
	TGraphErrors g_bitDeltaRel(g_bitDelta.GetN());
	g_bitDeltaRel.SetNameTitle((TString (g_bitDelta.GetN())).ReplaceAll("dnl_","bitdelta_rel_"),"Relative Bit Delta");
	//calculate relative delta errors
	for(int bit=0; bit<g_bitDelta.GetN();bit++){
		g_bitDeltaRel.SetPoint(bit,bit,		g_bitDelta.GetY()[bit]/(1<<bit));
		g_bitDeltaRel.SetPointError(bit,0,	g_bitDelta.GetEY()[bit]/(1<<bit));
	}

	return g_bitDeltaRel;
}

TGraphErrors Dnl2BitDelta(TGraph* g_dnl){
	int nbits=TMath::Log2(g_dnl->GetN()+1);
	TGraphErrors g_bitDNL(nbits);
	g_bitDNL.SetNameTitle((TString (g_dnl->GetName())).ReplaceAll("dnl_","bitdnl_"),"Bit DNL");

	TGraphErrors g_bitDelta(nbits);
	g_bitDelta.SetNameTitle((TString (g_dnl->GetName())).ReplaceAll("dnl_","bitdelta_"),"Bit Delta");

	printf("BitDNL analysis: analysing %d bits\n",nbits);
	//get mean delta and dnl
	for(int bit=0; bit<nbits;bit++){
		for (int bin=(1<<bit);bin<g_dnl->GetN();bin+=(1<<bit+1)){
			printf("Bit #%d -> bin\t%d: %e\n",bit,bin-1,g_dnl->GetY()[bin-1]);
			if (g_dnl->GetY()[bin-1]<-0.998) {printf(" ->[ign]\n");continue;}
			g_bitDNL.GetY()[bit]+=g_dnl->GetY()[bin-1];
		}
		//calculate mean dnl for bit
		g_bitDNL.GetX()[bit]=bit;
		g_bitDNL.GetY()[bit]/=1<<(nbits-bit-1);
	}
	//get dnl sdev
	for(int bit=0; bit<nbits;bit++){
		for (int bin=(1<<bit);bin<g_dnl->GetN();bin+=(1<<bit+1)){
			if (g_dnl->GetY()[bin-1]<-0.998) {continue;}
			g_bitDNL.GetEY()[bit]+=TMath::Power(g_dnl->GetY()[bin-1] - g_bitDNL.GetY()[bit],2);
		}
		//calculate mean dnl for bit
		g_bitDNL.GetEY()[bit]=TMath::Sqrt(g_bitDNL.GetEY()[bit]);
		g_bitDNL.GetEY()[bit]/=1<<(nbits-bit-1);
	}

	//calculate delta errors Dx from DNLx
	//DNL7 = D7-D6-D5-D4-D3-D2-D1-D0
	//DNL6 =    D6-D5-D4-D3-D2-D1-D0
	//DNL5 =       D5-D4-D3-D2-D1-D0
	//DNL4 =          D4-D3-D2-D1-D0
	//DNL3 =             D3-D2-D1-D0
	//DNL2 =                D2-D1-D0
	//DNL1 =                   D1-D0
	//DNL0 =                      D0
	//--
	//D0   = DNL0
	//D1   = DNL1-D0
	//D2   = DNL2-D1-D0
	// ...
	for(int bit=0; bit<nbits;bit++){
		g_bitDelta.SetPoint(bit,bit,g_bitDNL.GetY()[bit]);
		g_bitDelta.SetPointError(bit,0,g_bitDNL.GetEY()[bit]*g_bitDNL.GetEY()[bit]);

		if(bit>0) for(int b=bit-1;b>=0;b--){
			g_bitDelta.GetY()[bit]-=g_bitDelta.GetY()[b];
			g_bitDelta.GetEY()[bit]+=TMath::Power(g_bitDelta.GetEY()[b],2);
		}
		g_bitDelta.GetEY()[bit]=TMath::Sqrt(g_bitDelta.GetEY()[bit]);
	}

	return g_bitDelta;
}

TGraphErrors Delta2INL(TGraphErrors* g_delta){
	//recalculate the inl code from dnls/deltas
	TGraphErrors g_inl_regen(1<<g_delta->GetN());
	g_inl_regen.SetNameTitle("inl_regen","DNL contributions");
	for(unsigned int i=0; i<g_inl_regen.GetN();i++){
		double inl=0;
		double inle2=0;
		for (int b=0;b<g_inl_regen.GetN();b++)
			if((i & (1<<b)) != 0){
				inl+=g_delta->GetY()[b];
				inle2+=g_delta->GetEY()[b]*g_delta->GetEY()[b];
			}
		g_inl_regen.SetPoint(i,i,inl);
		g_inl_regen.SetPointError(i,i,TMath::Sqrt(inle2));
	}
	return g_inl_regen;
}

TGraph INLdiff(TGraph* g1, TGraph* g2){
	if(g1->GetN()!=g2->GetN()) return TGraph(0);
	TGraph g_inl_diff(g1->GetN());
	for(unsigned int i=0; i<g_inl_diff.GetN();i++){
		g_inl_diff.SetPoint(i,		g1->GetX()[i],
						g1->GetY()[i] - g2->GetY()[i]);
		//g_inl_diff.SetPointError(i,	TMath::Sqrt(g1->GetEX()[i]*g1->GetEX()[i] + g2->GetEX()[i]*g2->GetEX()[i]),
		//				TMath::Sqrt(g1->GetEY()[i]*g1->GetEY()[i] + g2->GetEY()[i]*g2->GetEY()[i]));
	}
	g_inl_diff.SetMarkerColor(kRed);
	return g_inl_diff;
}

TGraphErrors INLdiff(TGraph* g1, TGraphErrors* g2){
	if(g1->GetN()!=g2->GetN()) return TGraphErrors(0);
	TGraphErrors g_inl_diff(g1->GetN());
	for(unsigned int i=0; i<g_inl_diff.GetN();i++){
		g_inl_diff.SetPoint(i,		g1->GetX()[i],
						g1->GetY()[i] - g2->GetY()[i]);
		g_inl_diff.SetPointError(i,	TMath::Sqrt(g2->GetEX()[i]*g2->GetEX()[i]),
						TMath::Sqrt(g2->GetEY()[i]*g2->GetEY()[i]));
		//g_inl_diff.SetPointError(i,	TMath::Sqrt(g1->GetEX()[i]*g1->GetEX()[i] + g2->GetEX()[i]*g2->GetEX()[i]),
		//				TMath::Sqrt(g1->GetEY()[i]*g1->GetEY()[i] + g2->GetEY()[i]*g2->GetEY()[i]));
	}
	g_inl_diff.SetMarkerColor(kRed);
	return g_inl_diff;
}

TGraphErrors INLdiff(TGraphErrors* g1, TGraphErrors* g2){
	if(g1->GetN()!=g2->GetN()) return TGraphErrors(0);
	TGraphErrors g_inl_diff(g1->GetN());
	for(unsigned int i=0; i<g_inl_diff.GetN();i++){
		g_inl_diff.SetPoint(i,		g1->GetX()[i],
						g1->GetY()[i] - g2->GetY()[i]);
		g_inl_diff.SetPointError(i,	TMath::Sqrt(g1->GetEX()[i]*g1->GetEX()[i] + g2->GetEX()[i]*g2->GetEX()[i]),
						TMath::Sqrt(g1->GetEY()[i]*g1->GetEY()[i] + g2->GetEY()[i]*g2->GetEY()[i]));
	}
	g_inl_diff.SetMarkerColor(kRed);
	return g_inl_diff;
}

