
//DAQ
#include "Klaus4Config.h"
#include "IBConfig.h"
#include "VCRemoteClient.h"
#include "config_helpers.h"
#include "HistogramDAQ_client.h"
#include "HistogrammedResults.h"
#include "Sequencer.h"

//device library
//#include "SCPIoUART.h"
//#include "VPulseGen.h"
//#include "Agilent33250a.h"
//#include "Agilent6614c.h"
//#include "Keithley2200.h"
//#include "Keithley6487.h"

//ROOT
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <stdlib.h>
#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <iterator>
using namespace std;

#define C_GREEN "\x1B[32m"
#define C_RESET "\x1B[0m"

#include <ctime>
#include <signal.h>
// ************************** Unmasking of channels has to be done in config file ***************************************
// Sweep options and propagated objects
struct{
	// **************************
	// Basic sweep params
	struct{
		//std::list<double> vHV1=Seq::RangeList<double>("44"); //NOMINAL:58.5
		//std::list<double> vHV1=Seq::RangeSize<double>(56,61,0.5); //NOMINAL:58.5
		std::list<unsigned short> Tth_gl=Seq::RangeSize<unsigned short>(1,5,1);
		std::list<unsigned short> Tth_f=Seq::RangeSize<unsigned short>(0,15,1);
	} opt;
} sweep;

#include "Setup.h"
#include "ADCMeasurement.h"

//SIGINT handler
void handler_sigint(int sig){
	printf("Caught SIGINT!\n");
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		(*it)->Write(0,TObject::kOverwrite);
	}
	exit(0);
}

void WriteTemperatures(const char* str,TFile& fout){
	printf("Reading Temperatures\n");	
	fout.cd();
	TObjString s(setup.dev.daq_histos[0]->ReadTemperaturesJSON().c_str());
	cout <<"Temperatures:" << setup.dev.daq_histos[0]->ReadTemperaturesJSON().c_str() << endl;
	s.Write(str);
}

int main (int argc, char *argv[]){
	if (argc<4){
		std::cerr << "Usage: scan <host> <configbasename> <outputfile>\n";
		return -1;
	}

	//Init signal handler
	signal(SIGINT,&handler_sigint);

	//Init setup device tree
	setup.dev.SetN(1);

	//Init interface board
	setup.dev.config_ib=new VCRemoteClient<TIBConfig>(argv[1],0);
	setup.dev.config_ib->SetParValue("system/clkenable",1);
	setup.dev.config_ib->SetParValue("system/aqu_ena",1);
	setup.dev.config_ib->ChipReset();

	//Init ASIC configuration & DAQ
	char filename[256];
	for (int i=0;i<setup.dev.config_asic.size();i++){
		setup.dev.config_asic[i]=new VCRemoteClient<TKLauS4Config>(argv[1],i+1); 
		sprintf(filename,"%s%d.txt",argv[2],i);
		printf("Reading file %s for asic %d\n",filename,i);
		setup.dev.config_asic[i]->ReadFromFile(filename);
		setup.dev.config_asic[i]->UpdateConfig();

		setup.dev.daq_histos[i]=NULL;
	//	setup.dev.daq_histos[i] = new HistogramDAQ(argv[1],9090);
	}
	printf("Loading configuration...\n");
	char dummy[64];
	setup.dev.config_asic[0]->IssueCommand('l',0,NULL,63,dummy);
	setup.dev.config_asic[0]->IssueCommand('l',0,NULL,63,dummy);

	//Init DAQ
	printf("DAQ init...\n");
	setup.dev.daq_hits=new EventListDAQ(argv[1],9090);
	setup.dev.daq_hits->RegisterQueue(5000,1);

	Seq::Settling::Request(1000000);

	////Init LED power supply
	//setup.dev.LED_supply = new Agilent6614c();
	//setup.dev.LED_supply->Setcom("/dev/ttyS0");
	//setup.dev.LED_supply->SetVolt(1);
	//setup.dev.LED_supply->On();
	setup.dev.config_ib->SetParValue("system/led_system",0);

	//Init HV power supply
	//setup.dev.HV1_supply = new Agilent6614c();
	//setup.dev.HV1_supply->Setcom("/dev/ttyS2");
	//setup.dev.HV1_supply = new Keithley2200();
	//setup.dev.HV1_supply->Setcom("/dev/usbtmc0");
	//setup.dev.HV1_supply->SetVolt(58.5);
	//setup.dev.HV1_supply->On();

//Init ROOT
	TFile fout(TString::Format("%s",argv[3]),"recreate");
	WriteTemperatures("temperature_start",fout);
	setup.trees.push_back(new TTree("tr_counters","counters"));
	klaus_cec_data* data = NULL;
	setup.trees[0]->Branch("CEC",&data);
//	setup.trees.push_back(new TTree("tr_histos","histograms"));
//	setup.trees.push_back(new TTree("tr_hits","acquisition"));
	for (auto t: setup.trees)
		t->AutoSave();

//	Seq::MeasurementCollection todo;
//	todo.Add("led-run",new Measurement(setup.trees[0],setup.trees[1],600e3));
	// Some configuration settings
	//Fix to HG mode
	int i=0;
	for (auto config: setup.dev.config_asic){
		SetParAllChannel(config,"HG_scale_select",1);	//1:1 scaling for HG
		SetParAllChannel(config,"branch_sel_config",3);	//autogain
		SetParAllChannel(config,"ADC_in_n_select",1); 	// inner ground
		SetParAllChannel(config,"ext_trigger_select",0); 	// external trigger OFF
		SetParAllChannel(config,"trigger_th_finetune",0);	// trigger finetune DAC=0
		SetParAllChannel(config,"trigger_LSB_scale",0);	// trigger finetune LSB no scale
		config->SetParValue("comparator_th_trig",0);		// set the trigger threshold to be zero
		SaveConfigInROOT(config,TString::Format("config_ASIC%d",i));
		i++;
	}
	//Save the IB state
	SaveConfigInROOT(setup.dev.config_ib,"config_IB");
	SaveConfigStatusInROOT(setup.dev.config_ib,"status_IB");

	Seq::Settling::Settle();
//	Seq::Sequence<double>(setup.trees,"vHV1","vHV1/D").LoopOver(sweep.opt.vHV1,
//	[&data](Seq::Sequence<double>* this_seq){
		//std::cout<<" vLED: "<<this_seq->GetObservable()<<std::endl;
		//setup.dev.HV1_supply->SetVolt(this_seq->GetObservable());
		//Seq::Settling::Request(1000000);
		//Seq::Sequence<unsigned short>(this_seq->GetTrees(),"thresh_gl","thresh_gl/s").LoopOver(sweep.opt.Tth_gl,
		Seq::Sequence<unsigned short>(setup.trees,"thresh_gl","thresh_gl/s").LoopOver(sweep.opt.Tth_gl,
			[&data](Seq::Sequence<unsigned short>* this_seq){
				std::cout<<" Global threshold DAC: "<<this_seq->GetObservable()<<std::endl;
				for(auto config: setup.dev.config_asic)
					config->SetParValue("bias/comparator_th_trig",this_seq->GetObservable());
				Seq::Sequence<unsigned short>(this_seq->GetTrees(),"thresh_fine","thresh_fine/s").LoopOver(sweep.opt.Tth_f,
					[&data](Seq::Sequence<unsigned short>* this_seq){
					std::cout<<"Fine threshold DAC: "<<this_seq->GetObservable()<<std::endl;
					for (auto config: setup.dev.config_asic){
						SetParAllChannel(config, "trigger_th_finetune",this_seq->GetObservable());
						config->UpdateConfig();
						config->UpdateConfig();
						Seq::Settling::Request(10000);
					}
					//Measurements
					setup.dev.daq_hits->ResetCEC();
					setup.dev.daq_hits->ReadCECAsyncStart();
					sleep(20);
					setup.dev.daq_hits->ReadChipAsyncStop();
					usleep(100000);
					data=(klaus_cec_data*)setup.dev.daq_hits->CommandRepliesObject( klaus_cec_data::Class(),"get cec");
					if(!data) return true;
					data->Print();	
					for(auto t: setup.trees)
						t->Fill();
					
					
					return true;//Fine Threshold
				});
				return true;
			});//Threshold
//		return true;
//	});//vHV
	//Disable LED system
	//setup.dev.config_ib->SetParValue("system/led_system",0);
	//HV back to nominal
	//setup.dev.HV1_supply->SetVolt(58.5);
	
        printf("Finish the scan\n");
	fout.cd();
	for(std::vector<TTree*>::iterator it=setup.trees.begin();it!=setup.trees.end();it++){
		if((*it))
			(*it)->Write(0,TObject::kOverwrite);
	}
	WriteTemperatures("temperature_end",fout);
	fout.Close();
	return 0;
}
